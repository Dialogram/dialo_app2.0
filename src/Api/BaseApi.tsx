import axios from 'axios';
const BaseUrl = process.env.REACT_APP_BACKEND_HOST || "https://api-dev.dialogram.fr/";
export default abstract class BaseApi {

    static async call<T>({ method, url, body }: { method: any, url: any, body: any }, token?: string) {
        return axios({
            method: method,
            url: url,
            baseURL: BaseUrl,
            data: body,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization': `Bearer ${token}`
            },
        }).catch(error => {
            console.log("ERROR", error.response.data);
            return Promise.reject(error.response.data);
        }).then((response) => {
            //TODO Transformer response en T et le renvoyer 
            return Promise.resolve(response);
        });

    }
}
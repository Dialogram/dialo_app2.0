import axios from 'axios';

const BaseUrl = process.env.REACT_APP_BACKEND_HOST || "https://api-dev.dialogram.fr/";

export default class DialogramApi {
    static async call<T>({ method, url, body }: { method: any, url: any, body?: any }, token?: string) {
        return axios({
            method: method,
            url: url,
            baseURL: BaseUrl,
            data: body,
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest',
                'Authorization': `Bearer ${token}`
            },
        }).catch(error => {
            if (error && error.response)
                return Promise.reject(error.response.data);
            else
                return Promise.reject(error);
        }).then((response) => {
            return Promise.resolve({ data: response.data.data as T, includes: response.data.includes });
        });

    }
}
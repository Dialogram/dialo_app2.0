interface SourceI {
    type: string;
    uri: string;
}
class Source implements SourceI {
    type!: string;
    uri!: string;
    constructor(fields?: Partial<Source>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

class Asset {
    iframe!: string;
    player!: string;
    hls!: string;
    thumbnail!: string;
    constructor(fields?: Partial<Asset>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

class Access {
    role!: string;
    moderators!: Array<string>;
    collaborator!: Array<string>;
    constructor(fields?: Partial<Access>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export default class Video {
    id!: string;
    idOwner!: string;
    idApiVideo!: string;
    title!: string;
    public!: boolean;
    category!: string;
    publishedAt!: string;
    tags!: Array<any>;
    metadata!: Array<any>;
    source!: Source;
    assets!: Asset;
    timestamp!: number;
    access!: Access;
    constructor(fields?: Partial<Video>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
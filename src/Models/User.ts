
class Features {
    follows!: Array<number>;
    followers!: Array<number>;
    documentsLiked!: Array<number>;
    documentsCommented!: Array<number>;
    documentsFavorite!: Array<number>;

    constructor(fields?: Partial<Features>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

class Profile {
    registerDate!: string;
    createdWith!: any;
    firstName!: string;
    lastName!: string;
    profilePicture!: any;
    birthday!: string;
    certificated!: boolean ;
    country!: string ;
    description!: string ;
    gender!: string ;
    hometown!: string ;

    constructor(fields?: Partial<Profile>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export default class User {

    email!: string;
    features!: Features;
    id!: string;
    nickName!: string;
    profile!: Profile;
    role!: string;
    timestamp!: number;
    type!: string;

    constructor(fields?: Partial<User>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
    /* constructor({ email, features, id, nickName, profile, role, timestamp, type }: {
         email?: string, features?: any, id?: string, nickName?: string, profile?: any, role?: string, timestamp?: number, type?: string
     }) {
         this.email = email;
         this.features = new Features(features || {});
         this.id = id;
         this.nickName = nickName;
         this.profile = new Profile(profile || {});
         this.role = role;
         this.timestamp = timestamp;
         this.type = type;
     }*/
}
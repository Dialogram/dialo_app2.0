
class Features {
    likes!: Array<any>
    favorites!: Array<any>;
    comments!: Array<any>;

    constructor(fields?: Partial<Features>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export default class Doc {

    public type!: string;
    public id!: string;
    public name!: string;
    public link!: string;
    public usageName!: string;
    public nbPage!: number;
    public publicDoc!: boolean;
    public description!: string;
    public status!: string;
    public category!: string;
    public idTranslationMaster!: string;
    public idMasterTranslation!: string;
    public translationHistory!: Array<any>;
    public idOwner!: string;
    public creationDate!: string;
    public editDate!: string;
    public timestamp!: number;
    public access!: string;
    public features!: Features;

    constructor(fields?: Partial<Doc>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }

}
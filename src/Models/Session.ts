export default class Session {

    device: any;
    expireAt!: number;
    id!: string;
    provider!: string;
    refreshToken!: string;
    token!: string;
    type!: string;
    user!: string;

    constructor(fields?: Partial<Session>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }

}
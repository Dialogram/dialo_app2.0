export default class Zone {
    idMaster: string | undefined;
    _id: string | undefined;
    x: number = 100;
    y: number = 100;
    width: number = 0;
    height: number = 0;
    idVideo: string | undefined;
    page!: number;
    state: string | undefined;

    constructor(fields?: Partial<Zone>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }

}
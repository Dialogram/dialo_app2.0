
import Session from './Session';
import User from './User';
import Document from './Document';
import Video from './Video';

export default interface Store {
  user: User,
  session: Session,
  isConnected: boolean,
  token: string,
  createSession: any,
  setSession: any,
  getUser: any,
  userDocument: Array<Document>,
  userVideo: Array<Video>,
  exploreDocument: any,
  getMyDocument: any,
  getSingleDocument: any,
  createUser: any,
  logout: any,
  getMasterTraduction: any,
  updateDocument: any,
  getUserVideo: any,
  deleteVideo: any,
  deleteDocument: any,
  updateZone: any,
  deleteZone: any,
  createZone: any,
  searchDocumentByName: any,
  updatePublicInfo: any,
  updateAccountInfo: any,
  updatePassword: any,
  likeDocument: any,
  unLikeDocument: any,
  confirmAccount: any,
  sendConfirmAccount: any,
  promoteUser: any,
  searchUser: any,
  getUserById: any,
  demoteUser: any,
  socket: any,
  forgetPassword: any,
  resetPasswordWithToken: any
}
import Zone from "./Zone";

export default class Traduction {
    type!: string;
    id!: string;
    idDocument!: string;
    idOwner!: string;
    zones: Array<Zone> = [];
    version!: number;
    isMaster!: boolean;
    timestamp!: number;
    certificated!: boolean;
    idMasterForkedTranslation!: string;
    forkedTranslations!: Array<any>;
    isForked!: boolean;
    pushed!: boolean;

    constructor(fields?: Partial<Traduction>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}
import User from '../Models/User';

export function UserSerializer(data: Array<any>): User | Array<User> {
    let resp = data as Array<{}>;
    if (resp.length === 1) {
        let user = new User(data[0]);
        return user;
    } else {
        let users = [];
        for (let user in resp) {
            let s = new User(data[user])
            users.push(s)
        }
        return users;
    }
}
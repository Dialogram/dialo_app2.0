import Document from '../Models/Document';

export function DocumentSerializer(data: Array<any>): Array<Document> {
    let resp = data as Array<{}>;
    let documents = [];
    for (let doc in resp) {
        let s = new Document(data[doc])
        documents.push(s)
    }
    return documents;
}
import Video from '../Models/Video';

export function VideoSerializer(data: Array<any>): Array<Video> {
    let resp = data as Array<{}>;
    let videos = [];
    for (let vid in resp) {
        let s = new Video(data[vid])
        videos.push(s)
    }
    return videos;
}
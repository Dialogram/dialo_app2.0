import Session from '../Models/Session';

export function SessionSerializer(data: Array<any>): Session | Array<Session> {
    let resp = data as Array<{}>;
    if (resp.length === 1) {
        let session = new Session(data[0]);
        return session;
    } else {
        let sessions = [];
        for (let session in resp) {
            let s = new Session(data[session])
            sessions.push(s)
        }
        return sessions;
    }
}
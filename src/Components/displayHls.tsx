import React from 'react';
import styled from 'styled-components';
import ReactPlayer from 'react-player';
import { Icon } from 'antd';


const DisplayHls = (props: any) => {
    if (props.url === undefined)
        return null;
    return (
        <Container>
            <ContainerHeader>
                <Title>Vidéo HLS</Title> <CloseIcon onClick={() => props.closeHls()} type="close" />
            </ContainerHeader>
            <ReactPlayer url={props.url} controls width="395px" height="260px" playing={true} loop={true} />
        </Container>
    )
}

export default DisplayHls;
const Title = styled.h2`
`;
const CloseIcon = styled(Icon)`
    font-size: 1.5em;
    margin: 10px;
`;

const Container = styled.div`
    width: 395px;
    height: 260px;
    background:white;
    position:absolute;
    z-index: 80;
    bottom:20px;
    right:20px;
    display: flex;
    flex-wrap:wrap;
    flex-direction:columns;
    border: 1px solid rgba(85, 142, 227, 0.21);
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
`;

const ContainerHeader = styled.div`
    flex: 1;
    width: 100%;
    height: 30px;
    display:flex;
    justify-content: space-between;
    line-height:30px;
    padding-left: 10px;
    padding-right: 10px;
`;
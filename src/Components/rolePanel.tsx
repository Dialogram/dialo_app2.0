import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Drawer, Checkbox, Select, Modal, notification } from 'antd';
import styled from 'styled-components';
import { IconInput, TextArea } from './styled/input';
import { StoreConsumer } from "../Context";
import SearchUser from './searchUser';
import User from '../Models/User';
import Doc from '../Models/Document';
const { Option } = Select;

const RolePanel = withRouter((props: any) => {

    const [visible, setVisible] = useState(false);
    const [user, setUser] = useState();

    useEffect(() => {

    }, [])

    const failNotif = () => {
        notification.error({
            message: 'Modification refusé',
            description:
                'Une erreur est survenue lors du changement de role',
            duration: 2
        });
    };

    const successNotif = (desc: string) => {
        notification.success({
            message: 'Promotion réussi',
            description:
                desc,
            duration: 2
        });
    };

    const selectedUser = (user: User) => {
        console.log("selected 2", user);
        setUser(user);
    }
    const toCollaborator = (e: any, user: User) => {
        console.log(user.id, props.document.id)
        props.store.promoteUser(props.document.id, user.id, "collaborator").catch((e: any) => {
            failNotif();
            return Promise.reject();
        }).then((val: Doc[]) => {
            props.refreshDoc(val[0]);
            successNotif("L'utilisateur est maintenant collaborateur")
        })
    }

    const toModerator = (e: any, user: User) => {
        props.store.promoteUser(props.document.id, user.id, "moderator").catch((e: any) => {
            failNotif();
            return Promise.reject();
        }).then((val: Doc[]) => {
            props.refreshDoc(val[0]);
            successNotif("L'utilisateur est maintenant modérateur")
        })
    }

    return (
        <Drawer
            title={<HeaderTitle><h2>Gérer les droits</h2></HeaderTitle>}
            placement="right"
            closable={props.closable}
            onClose={props.onClose}
            visible={props.isVisible}
            width={500}
        >
            <StoreConsumer>
                {(store: any) => <SearchUser selectedUser={selectedUser} store={store} />}
            </StoreConsumer>

            {user && (
                <div>
                    {`${user.email} | ${user.nickName}`}
                    <button onClick={e => toModerator(e, user)}>promouvoir modérateur</button>
                    <button onClick={e => toCollaborator(e, user)}>promouvoir collaborateur</button>
                </div>)}
        </Drawer>
    )
});

export default RolePanel;


const HeaderTitle = styled.div`
    width: 100%;
    height: 40px;
    display:flex;
    justify-content space-between;
`;

const FormContainer = styled.div<any>`
    width: 400px;
    border-bottom: 1px solid #335AF0;
    margin-top: 20px;
    margin-bottom:20px;
    display:flex;
    flex-direction:column;
    align-content:space-between;
`;

const ErrorText = styled.p`
    color: red;
`;

const Button = styled.button<any>`
    border:none;
    width: 400px;
    height:36px;
    border-radius: 30px;
    cursor:pointer;
    color:white;
    text-align:center;
    margin: auto;
    margin-top: 30px;
    line-height:36px;
    background: rgb(84,144,228);
    background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
`;
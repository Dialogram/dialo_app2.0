import React from 'react';
import { withRouter } from 'react-router-dom';
import { Drawer, Checkbox, Select, Upload, Button, Icon, message } from 'antd';
import useDocumentFormUpload from '../Context/useDocumentFormUpload';
import styled from 'styled-components';

import { BaseUrl } from '../Context/store';

import { IconInput, TextArea } from './styled/input';

const { Option } = Select;
const VideoFormUpload = withRouter((props: any) => {
    const { form, handleChange, onBlur, handleTargetChecked, handleChangeSelected } = useDocumentFormUpload();

    const isValidForm = () => {
        if (form.nameValid === true && form.descriptionValid) {
            return false;
        }
        return true;
    }
    const uploadProps = {
        name: 'video',
        multiple: true,
        action: `${BaseUrl}api/translationVideo`,
        data: {
            "title": form.name,
            "description": form.description,
            "public": form.public,
            "category": form.category
        },
        headers: {
            Authorization: `Bearer ${props.token}`,
        },
        onChange(info: any) {
            console.log("onChange")
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
                console.log("SUCCESS UPLOAD")
                props.uploadSuccess();
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    return (
        <Drawer
            title="Ajouter une video"
            placement="right"
            closable={props.closable}
            onClose={props.onClose}
            visible={props.isVisible}
            width={500}
        >
            <p>Veuillez remplir tous les champs pour procéder au téléchargement de votre vidéo</p>
            <FormContainer width={props.width}>
                <ErrorText>{props.error}</ErrorText>
                <ErrorText>{form.nameError}</ErrorText>
                <IconInput value={form.name} placeholder={"Nom"} iconType={"user"} onChange={handleChange} name="name" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.descriptionError}</ErrorText>
                <TextArea value={form.description} placeholder={"Description"} onChange={handleChange} name="description" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <Select
                    defaultValue="administrative"
                    style={{ width: 200 }}
                    placeholder="Categories"
                    onChange={handleChangeSelected}
                >
                    <Option value="administrative">Administratif</Option>
                    <Option value="finance">Finance</Option>
                    <Option value="health">Santé</Option>
                    <Option value="entertainment">Divertissement</Option>
                    <Option value="business">Affaires</Option>
                </Select>
            </FormContainer>
            <Checkbox checked={form.public} name='public' onChange={handleTargetChecked}>Public</Checkbox >
            <Upload disabled={isValidForm()} {...uploadProps}>
                <Button>
                    <Icon type="upload" /> Ouvrir
                </Button>
            </Upload>

        </Drawer>
    )
});

export default VideoFormUpload;

const FormContainer = styled.div<any>`
    width: 400px;
    border-bottom: 1px solid #335AF0;
    margin-top: 20px;
    margin-bottom:20px;
    display:flex;
    flex-direction:column;
    align-content:space-between;
`;

const ErrorText = styled.p`
    color: red;
`;

import React, { useState } from 'react';
import styled from 'styled-components';
import { Document, Page, pdfjs } from 'react-pdf';
import { Icon } from "antd";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const Container = styled.div`
width: 200px;
height:250px;
border: 1px solid rgba(85, 142, 227, 0.21);
box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
overflow:hidden;
`;

const ContainerHeader = styled.div`
width: 200px;
height: 20px;
display:flex;
flex-direction:row;
justify-content: space-between;
align-items:center;;
padding-left:10px;
padding-right:10px;
`;

const P = styled.p`
margin:0;
padding:0;
margin-right:10px;
`;

const Span = styled.span`
display: flex;
flex-direction:row;
`;
const ContainerFooter = styled.div`
width: 200px;
height:35px;
`;

const DocTitle = styled.p`
font-size: 14px;
color:black;
margin:0;
padding:0;
margin-left:10px;
max-width:180px;
height:20px;
overflow:hidden;
`;
const DocCat = styled.p`
font-size:10px;
color:grey;
margin:0;
padding:0;
margin-top:-5px;
margin-left:10px;
`;
//require("../assets/pdf-test.pdf")
//props.doc.link.replace(/https:\/\/localhost/, 'http://localhost:8085')

const DocumentOverride = styled(Document)`
height: 193px;
overflow: hidden;
`;

const LikedHeart = styled(Icon)`
color: #AE149F;
line-height: 26px;
`;

const UnLikedHeart = styled(Icon)`
line-height: 26px;
`;
export default function DocumentCard(props: any) {
    const [pageNumber, setPageNumber] = useState(1);

    const onDocumentLoadSuccess = ({ numPages }: { numPages: number }) => {
        setPageNumber(numPages);
    };

    const isLiked = (): boolean => {
        if (props.user && props.user.id && props.doc) {
            return props.doc.features.likes.includes(props.user.id);
        }
        return false;
    }

    const like = (e: any) => {
        console.log("like");
        props.like(props.doc);
    }

    const unlike = (e: any) => {
        console.log("unlike");
        props.unLike(props.doc);
    }
    const fixCors = () => {
        if (document) {
            const splitted = props.doc.link.split('localhost');
            return `${splitted[0]}/localhost:8085${splitted[1]}?accessToken=${props.token}`
            //file={props.doc.link + '?accessToken=' + props.token}>
        }
        return '';
    }
    return (
        <Container>
            <ContainerHeader>

                <Span>
                    <P>{props.doc.features.likes.length}</P>
                    {isLiked() === true ? <LikedHeart type="heart" onClick={e => { unlike(e) }} /> : < UnLikedHeart onClick={e => { like(e) }} type="heart" />}
                </Span>
                {props.doc.status !== -1 && <Icon type="check" />}
            </ContainerHeader>
            <div onClick={(e: any) => { props.goToDoc(e, props.doc.id) }} >
                <DocumentOverride
                    onLoadSuccess={onDocumentLoadSuccess}
                    onLoadError={console.error}
                    file={props.doc.link + '?accessToken=' + props.token}>
                    <Page width={198} height={228} pageNumber={pageNumber} />
                </DocumentOverride>
            </div>
            <ContainerFooter>
                <DocTitle>{props.doc.name}</DocTitle>
                <DocCat>{props.doc.category}</DocCat>
            </ContainerFooter>
        </Container>
    )
}

import React, { Component } from 'react';
import styled from 'styled-components';
import { Icon, Avatar } from 'antd';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { List } from 'antd';
import Doc from '../Models/Document';
import { Link, withRouter } from 'react-router-dom';

class Search extends Component<any, any> {

    private onSearch$: Subject<any>;
    private subscription: Subscription | undefined;
    constructor(props: any) {
        super(props);
        this.state = {
            search: '',
            debounced: '',
            doc: []
        };
        this.onSearch$ = new Subject();
    }

    componentDidMount() {
        this.subscription = this.onSearch$
            .pipe(debounceTime(500))
            .subscribe(debounced => {
                console.log(debounced);
                this.props.store.searchDocumentByName(debounced).catch((e: any) => console.log(e)).then((val: any) => {
                    this.setState({
                        ...this.state,
                        doc: val
                    })
                })
                this.setState({ debounced })
            });
    }

    componentWillUnmount() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onChange = (e: any) => {
        const { value } = e.target;
        this.setState({ search: value });
        this.onSearch$.next(value);
    }
    goToDoc = (e: any, id: string) => {
        //<Link to={`/document/${item.id}`}>
        this.setState({ doc: [] });
        this.props.history.push(`/document/${id}`);

    }

    render() {
        const { search } = this.state;
        return (
            <SearchOutDiv>
                <SearchInput onChange={this.onChange} value={search} name="search" type='text' placeholder="Rechercher un document" />
                <InputButton><Icon type="search" /></InputButton>
                {this.state.doc.length > 0 &&
                    <ContentData>
                        <List
                            dataSource={this.state.doc}
                            renderItem={(item: Doc) => (
                                <List.Item key={item.id} onClick={e => this.goToDoc(e, item.id)}>
                                    <List.Item.Meta
                                        avatar={<Avatar src={item.link} />}
                                        title={item.name}
                                        description={item.description}
                                    />
                                </List.Item>
                            )}
                        >
                        </List>
                    </ContentData>}
            </SearchOutDiv>)
    }
}

export default withRouter(Search);

const ContentData = styled.div`
    width: 252px;
    min-height: 100px;
    background: white;
    position: absolute;
    z-index: 9999;
    top: 40px;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    padding-left: 10px;
    padding-right: 10px;
    overflow:hidden;
`;

const SearchOutDiv = styled.div`
    border-radius: 40px;
    height: 34px;
    width: 250px;
    padding:1px;
    background: rgb(66,24,187);
    background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
    display:flex;
`;

const SearchInput = styled.input`
    border-radius: 40px;
    height: 32px;
    width: 248px;
    margin: 0;
    padding: 0;
    background: white;
    box-shadow: none;
    border: none;
    padding-left: 20px;
`;

const InputButton = styled.div`
    border-top-right-radius:40px;
    border-bottom-right-radius:40px;
    height: 34px;
    width: 34px;
    background: rgb(66,24,187);
    background: linear-gradient(168deg,rgba(66,24,187,1) 43%,rgba(85,142,227,1) 95%);
    color: white;
    position: relative;
    margin-left: -28px;
    margin-top: -1px;
    font-size: 18px;
    display: flex;
    align-items: center;
    justify-content: center;
`;
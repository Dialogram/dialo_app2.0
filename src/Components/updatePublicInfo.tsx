import React, { useEffect } from 'react';
import { Input, Form, Button, Radio, DatePicker } from 'antd';
import Styled from 'styled-components';
import moment from 'moment';

const UpdatePublicInfo = (props: any) => {

    /*useEffect(() => {
        props.form.setFieldsValue({
           firstName : props.data.firstName
        });
    }, [])*/
    const handleSubmit = (e: any) => {
        e.preventDefault();
        props.form.validateFields((err: any, values: any) => {
            if (!err) {
                console.log('Received values of form: ', values);
                values.birthday = values.birthday._i;
                props.onSubmit(values);
            }
        });
    }
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Item>
                {props.form.getFieldDecorator('firstName', {
                    rules: [{ required: true, message: 'Ne peut pas être vide.' }],
                    initialValue: props.data.firstName

                })(
                    <Input addonBefore="Prénom" />
                )}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('lastName', {
                    rules: [{ required: true, message: 'Ne peut pas être vide.' }],
                    initialValue: props.data.lastName
                })(
                    <Input addonBefore="Nom" />
                )}
            </Form.Item>
            <Form.Item label="Sexe">
                {props.form.getFieldDecorator('gender',
                    {
                        rules: [{ required: true, message: "doit etre renseigné" }],
                        initialValue: props.data.gender
                    }
                )(
                    <Radio.Group>
                        <Radio value="male">Homme</Radio>
                        <Radio value="female">Femme</Radio>
                    </Radio.Group>,
                )}
            </Form.Item>
            <Form.Item label="Date de naissance">
                {props.form.getFieldDecorator('birthday',
                    {
                        rules: [{ type: 'object', required: true, message: 'Renseignez votre date de naissance' }],
                        initialValue: moment(props.data.birthday, 'DD/MM/YYYY')
                    }
                )(<DatePicker />)}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('country', {
                    rules: [{ required: true, message: 'Ne peut pas être vide.' }],
                    initialValue: props.data.country
                })(
                    <Input addonBefore="Pays" />
                )}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('hometown', {
                    rules: [{ required: true, message: 'Ne peut pas être vide.' }],
                    initialValue: props.data.hometown
                })(
                    <Input addonBefore="Ville" />
                )}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('description', {
                    rules: [{ required: true, message: 'Ne peut pas être vide.' }],
                    initialValue: props.data.description
                })(
                    <Input addonBefore="Déscription" />
                )}
            </Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
                Modifier
          </Button>
        </Form>
    );
}

export default Form.create<any>({ name: 'register' })(UpdatePublicInfo);

const EditAction = Styled.p`
cursor:pointer;
color: rgba(66,24,187,1);
padding:0;
margin:0;

`;

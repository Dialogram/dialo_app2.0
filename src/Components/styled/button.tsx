import React from 'react';
import styled from 'styled-components';
import { Icon } from 'antd';

const ButtonFilled = styled.button<any>`
   border-radius: 40px;
   height: 30px;
   border: none;
   padding-left:10px;
   padding-right:10px;
   line-height:10px;
   background: rgb(66,24,187);
   background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
   color: white;
   font-size: 12px;
   display:flex;
   flex-direction:row;
   align-items:center;
   cursor: pointer;
   :target{
       border:none;
   }
   :active{
       border:none;
   }
   display: ${ props => props.hide === true ? "none" : ""}
`;

const P = styled.p`
margin:0;
margin-left: 10px;
line-height: 25px;
`;

const Span = styled.span`
line-height:25px;
`;

const FilledIconButton = (props: { id?: string, hide?: boolean, iconType: string, label: string, onClick?: (e: any) => void, type: "button" | "reset" | "submit" }, className?: string) => (
    <ButtonFilled id={props.id} hide={props.hide} onClick={props.onClick} type={props.type}>
        <Span>
            <Icon type={props.iconType} />
        </Span>
        <P>{props.label}</P>
    </ButtonFilled>
)

const HollowButtonA = styled.a`
background: linear-gradient(to right,#4218BB,#558EE3);
border-radius: 8px;
color: #4218BB;
display: inline-block;
font-size: 20px;
padding: 1px;
text-decoration: none;

`;

const HollowSpan = styled.span`
background: #fff;
display: block;
padding: 0;
border-radius: 6px;
font-size: 12px;
padding-left: 10px;
padding-right: 10px;
height: 30px;
line-height: 30px;
display:flex;
flex-direction:row;
align-items:center;
cursor: pointer;
`;

const HollowIconButton = (props: { href: string, onClick?: (e: any) => void, iconType: string, label: string }) =>
    (<HollowButtonA href={props.href} onClick={props.onClick}>
        <HollowSpan><Icon type={props.iconType} /><P>{props.label}</P></HollowSpan>
    </HollowButtonA>
    )

const DivOutIconButton = styled.div<{ hide?: any, hideBorder?: any }>`
height:34px;
width: 34px;
margin:0;
padding:1px;
border-radius: 40px;
background: ${props => props.hideBorder ? 'transparent' : 'linear-gradient(to right,#4218BB,#558EE3);'}
display: ${ props => props.hide === true ? "none" : ""}
`;
const DivInIconButton = styled.div<any>`
background: white;
margin: auto;
height: 32px;
width: 32px;
border-radius: 40px;
color: #4218BB;
font-size: 18px;
padding: 1px;
display: flex;
justify-content: center;
align-items: center;
cursor: pointer;
`;
const IconButton = (props: any) => (
    <DivOutIconButton hide={props.hide} onClick={props.onClick} hideBorder={props.hideBorder}>
        <DivInIconButton><Icon type={props.iconType} /></DivInIconButton>
    </DivOutIconButton>
)


export {
    ButtonFilled,
    FilledIconButton,
    HollowIconButton,
    IconButton
};
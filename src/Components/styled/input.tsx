import React from 'react';
import styled from 'styled-components';
import { Icon } from 'antd';

const Input = styled.input`
width: 100%
height:30px;
    border:none;
    font-size: 18px;
    padding-left: 20px;
    color:#335AF0;
`;

const TextArea = styled.textarea`
width: 100%
    border:none;
    font-size: 18px;
    padding-left: 20px;
    color:#335AF0;
`;

const IconInputContainer = styled.div`
    display:flex;
    flex-direction:row;
    justify-content: flex-start;
    align-items: center;
    font-size:16px;
    color: #335AF0;
    padding-left: 10px;
`;
const IconInput = (props: any) => (
    <IconInputContainer>
        <Icon type={props.iconType} />
        <Input id={props.id} type={props.type} placeholder={props.placeholder} onChange={props.onChange} value={props.value} name={props.name} onBlur={props.onBlur} />
    </IconInputContainer>
)

const IconTextArea = (props: any) => (
    <IconInputContainer>
        <Icon type={props.iconType} />
        <TextArea placeholder={props.placeholder} onChange={props.onChange} value={props.value} name={props.name} onBlur={props.onBlur} />
    </IconInputContainer>
)


export {
    Input,
    IconInput,
    IconTextArea,
    TextArea
}
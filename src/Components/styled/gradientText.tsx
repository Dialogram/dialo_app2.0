import styled from 'styled-components';

const GradientText = styled.p`
background:linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
-webkit-background-clip: text;
-webkit-text-fill-color: transparent;
`;
export{
    GradientText
}
import React, { useState } from 'react';
import styled from 'styled-components';

import { Icon } from "antd";


const Container = styled.div`
width: 200px;
height:250px;
border: 1px solid rgba(85, 142, 227, 0.21);
box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
overflow:hidden;
display:flex;
flex-direction: column;
justify-content:center;
`;



const MyIcon = styled(Icon)`
flex: 1;
font-size: 3em;
line-height: 250px;
`;

export default function DocumentCardEmpty(props: any) {
    return (
        <Container>

            <MyIcon onClick={(e: any) => {
                e.preventDefault();
                props.goToUploadDoc();
            }} type="plus" />

        </Container>
    )
}
import React, { useState, useEffect } from 'react';
import { Icon } from 'antd';
import Styled from 'styled-components';
import DocumentCard from './document-card';
import Doc from '../Models/Document';
import { withRouter } from 'react-router-dom';
import DocumentCardEmpty from './document-card-empt';
import DocumentFormUpload from './documentFormUpload';

const MyDocumentSlider = withRouter((props: any) => {

    const [range, setRange] = useState({ from: 0, to: 4 });
    const [documents, setDocument] = useState([]);
    const [drawerVisible, setDrawerVisible] = useState(false);
    const [error, setError] = useState("");

    useEffect(() => {
        /* props.store.exploreDocument({ limit: 50, page: 0, category: props.category }).then((val: any) => {
             console.log(props.category, val)
             setDocument(val);
         })*/
        props.store.getMyDocument().then((val: any) => {
            console.log("MY DOCUMENT", props.category, val)
            setDocument(val);
        })
    }, [])

    const nextDoc = () => {
        setRange({ from: range.from + 4, to: range.to + 4 })
    };
    const prevDoc = () => {
        setRange({ from: (range.from - 4) < 0 ? 0 : range.from - 4, to: (range.to - 4) < 4 ? 4 : range.to - 4 })
    }

    const goToDocument = (e: any, id: string) => {
        props.history.push(`/document/${id}`);
    }
    const updateDoc = (id: string, body: any) => {
        props.store.updateDocument(id, body).then(
            (doc: Doc) => {
                setDrawerVisible(false);
            }, (error: { error: string }) => {
                console.log(error);
                setError(error.error)
            })
    }
    const like = (doc: Doc) => {
        props.store.likeDocument(doc).then((val: Doc) => {
            console.log(val);
            props.store.exploreDocument({ limit: 50, page: 0, category: props.category }).then((val: any) => {
                console.log(props.category, val)
                setDocument(val);
            })
        })
    }

    const unLike = (doc: Doc) => {
        props.store.unLikeDocument(doc).then((val: Doc) => {
            console.log(val);
            props.store.exploreDocument({ limit: 50, page: 0, category: props.category }).then((val: any) => {
                console.log(props.category, val)
                setDocument(val);
            })
        })
    }
    const goToploadDoc = () => {
        setDrawerVisible(true);
    }
    return (
        <React.Fragment>
            <DocumentFormUpload token={props.store.session.token} error={error} onSubmit={updateDoc} width={props.width} closable={false} onClose={(e: any) => { setDrawerVisible(false) }} isVisible={drawerVisible}></DocumentFormUpload>
            <MediumTitle>{props.title}</MediumTitle>
            <Row width={props.width}>
                {props.store.userDocument.length === 0 ? <CardContainer> <DocumentCardEmpty goToUploadDoc={goToploadDoc} /> </CardContainer> : range.from > 0 && <LeftArrow onClick={prevDoc}><Icon type="left" /></LeftArrow>}
                {props.store.userDocument.map((value: Doc, index: number) => {

                    if (index >= range.from && index <= range.to) {
                        return (<CardContainer key={index} ><DocumentCard goToDoc={goToDocument} like={like} unLike={unLike} doc={value} token={props.store.session.token} user={props.store.user} /></CardContainer>)
                    } else {
                        return null;
                    }
                })}
                {(range.to < props.store.userDocument.length && props.store.userDocument.length % 4 !== 0) && <RightArrow onClick={nextDoc}><Icon type="right" /></RightArrow>}
            </Row>
        </React.Fragment>

    )
});

export default MyDocumentSlider;
const RightArrow = Styled.div`
width: 40px;
height: 40px;
background: rgb(66,24,187);
background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
border-radius: 40px;
position: relative;
z-index: 4;
left: -13px;
padding: 13px;
color: white;
line-height: 14px;
cursor:pointer;
`;
const LeftArrow = Styled.div`
width: 40px;
height: 40px;
background: rgb(66,24,187);
background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
border-radius: 40px;
position: relative;
z-index: 4;
left:15px;
padding: 13px;
color: white;
line-height: 14px;
cursor:pointer;
`;

const MediumTitle = Styled.h3`
font-size:16px;
margin:0;
padding:0;
padding-left:10px;
color: #AE149F;
margin-bottom:10px;
margin-top:10px;
font-size: 1.4em;
`;

const CardContainer = Styled.div<any>`
margin-left:20px;
margin-right:20px;
`;

const Row = Styled.div<any>`
width:${props => props.width < 768 ? '100%' : '1280px'};
display: flex;
flex-direction: ${props => props.width < 768 ? 'column' : 'row'};
justify-content: space-arround;
align-items: center;
`;

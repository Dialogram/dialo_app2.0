import React, { useState } from 'react';
import { Drawer, Icon } from 'antd';
import Video from '../Models/Video';
import styled from 'styled-components';
import VideoCard from './videoCard';
import { MediaCardType } from "../Models/MediaCardEnum";
import { FilledIconButton } from './styled/button';

export default function (props: any) {

    const [mediaType, setMediaType] = useState(MediaCardType.BIG)
    const toList = () => {

        setMediaType(MediaCardType.LIST);
    }

    const toBigImg = () => {
        setMediaType(MediaCardType.BIG);
    }

    const toMediumImg = () => {
        setMediaType(MediaCardType.MEDIUM);
    }
    return (
        <Drawer
            title={<HeaderContainer>
                <HeaderTitle>Vos videos</HeaderTitle>
                <FilledIconButton type='button' iconType="plus" label="Ajouter" onClick={(e: any) => { props.addVideo(); }}></FilledIconButton>
            </HeaderContainer>
            }
            placement="right"
            closable={false}
            onClose={props.onClose}
            visible={props.isVisible}
            width={500}
        >
            <ToolBar>
                <ToolIcon selected={mediaType === MediaCardType.LIST ? true : false} onClick={(e: any) => toList()} type="profile" />
                <ToolIcon selected={mediaType === MediaCardType.BIG ? true : false} onClick={(e: any) => toBigImg()} type="appstore" />
                <ToolIcon selected={mediaType === MediaCardType.MEDIUM ? true : false} onClick={(e: any) => toMediumImg()} type="table" />
            </ToolBar>
            <VideosContainer >
                {
                    Array.from(
                        props.videos && props.videos,
                        (el: Video, index) =>
                            (
                                <VideoCard onClick={(e: any) => {
                                    props.selectedVideo(el.id);
                                }} key={index} video={el} type={mediaType} onVideoDeleteConfirm={props.onVideoDeleteConfirm} />
                            ))
                }
            </VideosContainer>
        </Drawer>
    )
}
const HeaderTitle = styled.h2`
margin:0;
padding:0;
flex: 1;
`;
const HeaderContainer = styled.div`
width: 100%;
height: 40px;
display:flex;
justify-content: space-between;
padding-left:20px;
padding-right:20px;
`;
const ToolIcon = styled(Icon) <any>`
    font-size: 2em;
    margin-right: 20px;
    cursor:pointer;
    ${props => props.selected === true && 'color : #4218BB;'}
`;
const ToolBar = styled.div`
    width: 100%;
    height: 30px;
    display:flex;
    justify-content: flex-end;
    flex-direction:row;
    margin-bottom: 30px;
`;

const VideosContainer = styled.div<any>`
    display:flex;
    flex-wrap: wrap;
    flex-direction:row;
    justify-content: space-between;
`;

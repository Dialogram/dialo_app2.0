import React, { Component } from 'react';
import styled from 'styled-components';
import { Icon, Avatar } from 'antd';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { List } from 'antd';
import Doc from '../Models/Document';
import User from '../Models/User';

import { Popover, Button } from 'antd';

class SearchUser extends Component<any, any> {

    private onSearch$: Subject<any>;
    private subscription: Subscription | undefined;
    constructor(props: any) {
        super(props);
        this.state = {
            search: '',
            debounced: '',
            user: [],
            visible: false,
            clickedUser: undefined,
        };
        this.onSearch$ = new Subject();
    }

    componentDidMount() {
        this.subscription = this.onSearch$
            .pipe(debounceTime(500))
            .subscribe(debounced => {
                console.log(debounced);
                this.props.store.searchUser(debounced).catch((e: any) => console.log(e)).then((val: any) => {
                    console.log("users", val);
                    this.setState({
                        ...this.state,
                        user: val
                    })
                })
                this.setState({ debounced })
            });
    }

    componentWillUnmount() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onChange = (e: any) => {
        const { value } = e.target;
        this.setState({ search: value });
        this.onSearch$.next(value);
    }
    selectUser = (e: any, item: User) => {
        e.preventDefault();
        this.setState({
            ...this.state,
            user: [],
        })
          this.props.selectedUser(item);
    }

    hide = () => {
        this.setState({
            visible: false,
        });
    };

    handleVisibleChange = (visible: boolean) => {
        this.setState({ visible });
    };

  /*  content = (
        <div>
            <p onClick={e => this.toModerator(this.state.clickedUser)}>Modérateur</p>
            <p onClick={(e: any) => this.toCollaborator(this.state.clickedUser)} >Supprimer</p>
        </div>
    );

    toCollaborator = (user: User) => {
        this.props.store.promoteUser(this.props.document.id, user.id, "collaborator").then((val: Doc) => {
           // this.props.refreshDoc(val);
        })
    }

    toModerator = (user: User) => {
        this.props.store.promoteUser(this.props.document.id, user.id, "moderator").then((val: Doc) => {
            //this.props.refreshDoc(val);
        })
    }*/
    render() {
        const { search } = this.state;
        return (
            <React.Fragment>
                <SearchOutDiv>
                    <SearchInput onChange={this.onChange} value={search} name="search" type='text' placeholder="Rechercher un utilisateur" />
                    <InputButton><Icon type="search" /></InputButton>
                </SearchOutDiv>
                {this.state.user && this.state.user.length > 0 &&
                    <ContentData>
                        <List
                            dataSource={this.state.user}
                            renderItem={(item: User) => (

                                <List.Item key={item.id} onClick={e => { this.selectUser(e, item); e.preventDefault() }}>
                                    <List.Item.Meta
                                        avatar={<Avatar src={item.profile.profilePicture.url} />}
                                        title={item.nickName}
                                        description={item.email}
                                    />
                                </List.Item>
                            )}
                        >
                        </List>
                    </ContentData>}
            </React.Fragment>
        )
    }
}

export default SearchUser;

const ContentData = styled.div`
    width: 252px;
    min-height: 100px;
    background: white;
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    padding-left: 10px;
    padding-right: 10px;
    overflow:hidden;
`;

const SearchOutDiv = styled.div`
    border-radius: 40px;
    height: 34px;
    width: 250px;
    padding:1px;
    background: rgb(66,24,187);
    background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
    display:flex;
`;

const SearchInput = styled.input`
    border-radius: 40px;
    height: 32px;
    width: 248px;
    margin: 0;
    padding: 0;
    background: white;
    box-shadow: none;
    border: none;
    padding-left: 20px;
`;

const InputButton = styled.div`
    border-top-right-radius:40px;
    border-bottom-right-radius:40px;
    height: 34px;
    width: 34px;
    background: rgb(66,24,187);
    background: linear-gradient(168deg,rgba(66,24,187,1) 43%,rgba(85,142,227,1) 95%);
    color: white;
    position: relative;
    margin-left: -28px;
    margin-top: -1px;
    font-size: 18px;
    display: flex;
    align-items: center;
    justify-content: center;
`;
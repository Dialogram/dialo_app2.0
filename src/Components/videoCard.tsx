import React, { useState } from 'react';
import { MediaCardType } from "../Models/MediaCardEnum";
import styled from 'styled-components';
import { Icon, Modal } from 'antd';


const VideoCard = (props: any) => {

    const [visible, setVisible] = useState(false);

    const cardStyle = [
        {
            width: "200px"
        },
        {
            width: "100px"
        },
        {
            width: "100%"
        }
    ]

    const showModal = () => {
        setVisible(true);
        console.log("show modal")
    };
    const handleOk = () => {
        props.onVideoDeleteConfirm(props.video.idApiVideo);
        setVisible(false);
        console.log("OK")
    }

    const handleCancel = () => {
        setVisible(false);
        console.log("cancel")
    }

    return (
        <CardContainer cardStyle={cardStyle} mediaType={props.type}>
            <Modal
                title="Suppression d'une video"
                visible={visible}
                onOk={(e: any) => handleOk()}
                onCancel={(e: any) => handleCancel()}
            >
                <p>Attention vous allez supprimer cette vidéo, si vous confirmez vous ne pourrez pas revenir en arrière</p>
            </Modal>
            <ImageContainer onClick={(e: any) => { props.onClick(e) }} >
                <Image src={props.video.assets.thumbnail} />
            </ImageContainer >
            <MetaContainer titleOnly={props.type === MediaCardType.BIG || props.type === MediaCardType.MEDIUM} onClick={(e: any) => { props.onClick(e) }}>
                <Title>{props.video.title}</Title>
                <Description>{props.video.description}</Description>
            </MetaContainer>
            <ToolContainer>
                <ToolItemContainer>
                    <Icon type="edit" />
                </ToolItemContainer>
                <ToolDivider></ToolDivider>
                <ToolItemContainer onClick={(e: any) => { showModal() }}>
                    <Icon type="delete" />
                </ToolItemContainer>
            </ToolContainer>
        </CardContainer>
    )
}

const ToolItemContainer = styled.div`
width: 50%;
display: flex;
justify-content:center;
`;
const ToolDivider = styled.div`
    width: 1px;
    height: 25px;
    background: #e8e8e8;
`;

const ToolContainer = styled.div`
    width: 100%;
    height: 30px;
    background: #fafafa;
    border-top: 1px solid #e8e8e8;
    display:flex;
    justify-content: space-between;
    align-items: center;
`;

const Title = styled.h2`
    margin:0;
    padding:0;
    flex: 1;
    height: 30px;
    padding-left: 15px;
`;

const Description = styled.p`
    margin:0;
    padding:0;
    flex: 0 0 100%;
    padding-left: 15px;
    height:auto;
`;
const MetaContainer = styled.div<any>`
    flex: 1 1 auto;
    display:flex;
    flex-direction:column;
    overflow:hidden;
    ${props => props.titleOnly === true && 'height:30px'}
`;

const CardContainer = styled.div<any>`
    width: ${props => props.cardStyle[props.mediaType].width};
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    display:flex;
    margin-bottom:20px;
    flex-wrap:wrap;
`;

const ImageContainer = styled.div`
    width: 200px;
    height: auto;
`;

const Image = styled.img`
    display: block;
    width: 100%;
    height: auto;
`;

export default VideoCard;
import React from 'react';
import { Input, Form, Button } from 'antd';
import Styled from 'styled-components';

const UpdatePassword = (props: any) => {

    const handleSubmit = (e: any) => {
        e.preventDefault();
        props.form.validateFields((err: any, values: any) => {
            if (!err) {
                console.log('Received values of form: ', values);
                props.onSubmit({
                    currentPassword: values.currentPassword,
                    newPassword: values.newPassword
                });
            }
        });
    }
    const compareToFirstPassword = (rule: any, value: any, callback: any) => {
        const { form } = props;
        if (value && value !== form.getFieldValue('newPassword')) {
            callback('Two passwords that you enter is inconsistent!');
        } else {
            callback();
            props.onSubmit();
        }
    };
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Item>
                {props.form.getFieldDecorator('currentPassword', {
                    rules: [{ required: true, message: 'Ne peut être vide' }
                    ],

                })(
                    <Input.Password addonBefore="Mot de passe actuel" />
                )}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('newPassword', {
                    rules: [{ required: true, message: 'Ne peut être vide' }
                    ],

                })(
                    <Input.Password addonBefore="Nouveau mot de passe" />
                )}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('confirm', {
                    rules: [
                        {
                            required: true,
                            message: 'Please confirm your password!',
                        },
                        {
                            validator: compareToFirstPassword,
                        },
                    ],

                })(
                    <Input.Password addonBefore="Confirmation" />
                )}
            </Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
                Modifier
          </Button>
        </Form>
    );
}

export default Form.create<any>({ name: 'register' })(UpdatePassword);

const EditAction = Styled.p`
cursor:pointer;
color: rgba(66,24,187,1);
padding:0;
margin:0;

`;

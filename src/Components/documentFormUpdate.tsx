import React, { useEffect, useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Drawer, Checkbox, Select, Modal, List, Typography, notification, Icon } from 'antd';
import useDocumentFormUpdate from '../Context/useDocumentFormUpdate';
import styled from 'styled-components';
import { IconInput, TextArea } from './styled/input';
import { StoreConsumer } from '../Context';
import SearchUser from './searchUser';
import User from '../Models/User';
import Doc from '../Models/Document';

const { Option } = Select;

const DocumentFormUpdate = withRouter((props: any) => {

    const [visible, setVisible] = useState(false);
    const { form, handleChange, onBlur, handleTargetChecked, handleChangeSelected, setForm } = useDocumentFormUpdate();
    const [user, setUser] = useState();
    const [moderators, setModerators] = useState(new Array<User>());
    const [collaborators, setCollaborators] = useState(new Array<User>());
    useEffect(() => {
        setForm({
            ...form,
            name: props.document.name,
            nameValid: true,
            description: props.document.description,
            descriptionValid: true,
            category: props.document.category,
            public: props.document.publicDoc
        })
        getAllCollaboratorUserById(props.document.access.collaborator);
        getAllModeratorUserById(props.document.access.moderators);
    }, [props.document])

    const submit = (e: any) => {
        if (form.nameValid === true && form.descriptionValid === true) {
            props.onSubmit(props.document.id, {
                "name": form.name,
                "description": form.description,
                "public": form.public === true ? "true" : "false",
                "category": form.category
            })
        }
    }

    const showModal = () => {
        setVisible(true);
        console.log("show modal")
    };
    const handleOk = () => {
        props.deleteDocument(props.document.id);
        props.history.push('/')
        setVisible(false);
        console.log("OK")
    }

    const handleCancel = () => {
        setVisible(false);
        console.log("cancel")
    }
    const failNotif = () => {
        notification.error({
            message: 'Modification refusé',
            description:
                'Une erreur est survenue lors du changement de role',
            duration: 2
        });
    };

    const successNotif = (desc: string) => {

        notification.success({
            message: 'Promotion réussi',
            description:
                desc,
            duration: 2
        });
    };

    const selectedModeratorUser = (user: User) => {

        console.log("selected 2", user);
        toModerator(user);
    }

    const selectedCollaboratorUser = (user: User) => {

        console.log("selected 2", user);
        toCollaborator(user);
    }
    const toCollaborator = (user: User) => {
        console.log(user.id, props.document.id)
        props.store.promoteUser(props.document.id, user.id, "collaborator").catch((e: any) => {
            failNotif();
            return Promise.reject();
        }).then((val: Doc[]) => {
            props.refreshDoc(val);
            console.log("after role",val)
            successNotif("L'utilisateur est maintenant collaborateur");
        })
    }
    const toModerator = (user: User) => {
        props.store.promoteUser(props.document.id, user.id, "moderator").catch((e: any) => {
            failNotif();
            return Promise.reject();
        }).then((val: Doc[]) => {
            props.refreshDoc(val);
            console.log("after role", val);
            successNotif("L'utilisateur est maintenant modérateur")
        })
    }

    const getAllCollaboratorUserById = async (ids: Array<string> = []) => {
        const u = new Array<User>();
        for (let id of ids) {
            const usr = await props.store.getUserById(id)/*.then((val: User) => {
                console.log(val)
                u.push(val);
            })*/
            u.push(usr)
        }
        setCollaborators(u);
        console.log(u)

    }

    const getAllModeratorUserById = async (ids: Array<string> = []) => {
        const u = new Array<User>();

        for (let id of ids) {
            const usr = await props.store.getUserById(id)/*.then((val: User) => {
                console.log(val)
                u.push(val);
            })*/
            u.push(usr);
        }

        setModerators(u);
        console.log(u)

    }

    const demoteCollaboratorUser = (e: any, user: User) => {
        props.store.demoteUser(props.document.id, user.id, "collaborator").catch((e: any) => {
            failNotif();
            return Promise.reject();
        }).then((val: Doc[]) => {
            props.refreshDoc(val);
            console.log(val)
            successNotif("L'utilisateur n'est plus collaborateur");
        })
    }

    const demoteModeratorUser = (e: any, user: User) => {
        props.store.demoteUser(props.document.id, user.id, "moderator").catch((e: any) => {
            failNotif();
            return Promise.reject();
        }).then((val: Doc[]) => {
            props.refreshDoc(val);
            console.log(val)
            successNotif("L'utilisateur n'est plus modérateur");
        })
    }

    return (
        <Drawer
            title={<HeaderTitle><h2>Modifier un document</h2> <button onClick={(e: any) => { showModal() }}>Delete</button></HeaderTitle>}
            placement="right"
            closable={props.closable}
            onClose={props.onClose}
            visible={props.isVisible}
            width={500}
        >
            <Modal
                title="Suppression d'une video"
                visible={visible}
                onOk={(e: any) => handleOk()}
                onCancel={(e: any) => handleCancel()}
            >
                <p>Attention vous allez supprimer ce document, si vous confirmez vous ne pourrez pas revenir en arrière</p>
            </Modal>
            <FormContainer width={props.width}>
                <ErrorText>{props.error}</ErrorText>
                <ErrorText>{form.nameError}</ErrorText>
                <IconInput value={form.name} placeholder={"Nom"} iconType={"user"} onChange={handleChange} name="name" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.descriptionError}</ErrorText>
                <TextArea value={form.description} placeholder={"Description"} onChange={handleChange} name="description" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <Select
                    defaultValue="administrative"
                    style={{ width: 400 }}
                    placeholder="Categories"
                    onChange={handleChangeSelected}
                >
                    <Option value="administrative">Administratif</Option>
                    <Option value="finance">Finance</Option>
                    <Option value="health">Santé</Option>
                    <Option value="entertainment">Divertissement</Option>
                    <Option value="business">Affaires</Option>
                </Select>
            </FormContainer>
            <Checkbox checked={form.public} name='public' onChange={handleTargetChecked}>Public</Checkbox >
            <Button onClick={submit}>Modifier</Button>

            {props.document.idOwner === props.store.user.id &&
                <List
                    header={
                        <StoreConsumer>
                            {(store: any) => <SearchUser selectedUser={selectedModeratorUser} store={store} />}
                        </StoreConsumer>}
                    bordered
                    dataSource={moderators}
                    renderItem={(item: User) => (
                        <List.Item
                            actions={[<Icon type="delete" onClick={e => demoteModeratorUser(e, item)} />]}>
                            <Typography.Text mark>Moderateurs</Typography.Text> {item.nickName}
                        </List.Item>
                    )}
                />
            }
            {props.document.idOwner === props.store.user.id && <List
                header={
                    <StoreConsumer>
                        {(store: any) => <SearchUser selectedUser={selectedCollaboratorUser} store={store} />}
                    </StoreConsumer>}
                bordered
                dataSource={collaborators}

                renderItem={(item: User) => (
                    <List.Item
                        actions={[<Icon type="delete" onClick={e => demoteCollaboratorUser(e, item)} />]}
                    >
                        <Typography.Text mark>Collaborateurs</Typography.Text> {item.nickName}
                    </List.Item>
                )}
            />}

        </Drawer>
    )
});

export default DocumentFormUpdate;


const HeaderTitle = styled.div`
    width: 100%;
    height: 40px;
    display:flex;
    justify-content space-between;
`;

const FormContainer = styled.div<any>`
    width: 400px;
    border-bottom: 1px solid #335AF0;
    margin-top: 20px;
    margin-bottom:20px;
    display:flex;
    flex-direction:column;
    align-content:space-between;
`;

const ErrorText = styled.p`
    color: red;
`;

const Button = styled.button<any>`
    border:none;
    width: 400px;
    height:36px;
    border-radius: 30px;
    cursor:pointer;
    color:white;
    text-align:center;
    margin: auto;
    margin-top: 30px;
    line-height:36px;
    background: rgb(84,144,228);
    background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
`;
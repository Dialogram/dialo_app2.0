import React, { useState } from "react";
import { Menu, Dropdown } from "antd";
import { withRouter, Link } from "react-router-dom";
import styled from 'styled-components';
import Search from './search';
import '../assets/css/header.css';
import { IconButton, FilledIconButton } from './styled/button';
import useWindowDimensions from "../Context/useWindowDimensions";
import { GradientText } from './styled/gradientText';

import DocumentFormUpload from './documentFormUpload';
import Doc from "../Models/Document";
import { StoreConsumer } from "../Context";





const HeaderApp = withRouter((props: any) => {
  const { width } = useWindowDimensions();
  const [drawerVisible, setDrawerVisible] = useState(false);
  const [error, setError] = useState("");
  const logOut = () => {
    props.logout()
    props.history.push('/login');
  }
  const menuLogged = (
    <Menu>
      <Menu.Item>
        <UserPicture src={require('../assets/img/2020_logo_DIALOGRAM.png')} />
      </Menu.Item>
      <Menu.Item>
        <IconButton hide={width > 768} iconType={"message"} />
      </Menu.Item>
      <Menu.Item>
        <IconButton hide={width > 768} iconType={"bell"} />
      </Menu.Item>
      <Menu.Item>
        <IconButton onClick={logOut} hide={props.isConnected === false} iconType={"logout"} />
        <IconButton hide={props.isConnected === true} iconType={"login"} />
      </Menu.Item>
    </Menu>
  );
  const menuLogOf = (
    <Menu>
      <Menu.Item>
        <IconButton iconType={"login"} />
      </Menu.Item>
    </Menu>
  );

  const updateDoc = (id: string, body: any) => {
    props.store.updateDocument(id, body).then(
      (doc: Doc) => {
        setDrawerVisible(false);
      }, (error: { error: string }) => {
        console.log(error);
        setError(error.error)
      })
  }

  return (
    <Container>
      <StoreConsumer>
        {store => <DocumentFormUpload store={store} token={props.store.session.token} error={error} onSubmit={updateDoc} width={props.width} closable={false} onClose={(e: any) => { setDrawerVisible(false) }} isVisible={drawerVisible}></DocumentFormUpload>}
      </StoreConsumer>
      <ContainerInner>
        <div className="nav-bar">
          <div className="nav-bar-left">
            <Logo src={require('../assets/img/2020_logo_DIALOGRAM.png')} />
            {props.isConnected === true && <StoreConsumer>
              {store => <Search store={store} />}
            </StoreConsumer>}
            <Link to={"/explorer"}><Explorer hide={props.isConnected === false}>Explorer</Explorer></Link>
          </div>
          <div className="nav-bar-right">
            <UserPicture hide={width < 768 || props.isConnected === false} src={props.store.user.profile.profilePicture.url} />
            <Link to={"/account"}><UserName hide={width < 768 || props.isConnected === false}>{props.store.user.nickName}</UserName></Link>
            <Divider hide={width < 768 || props.isConnected === false} />
            <FilledIconButton hide={width < 768 || props.isConnected === false} onClick={(e: any) => { setDrawerVisible(true) }} iconType="plus" label="Nouveau" type="button"></FilledIconButton>
            <IconButton hide={width < 768 || props.isConnected === false} iconType={"message"} />
            <IconButton hide={width < 768 || props.isConnected === false} iconType={"bell"} />
            <Dropdown overlay={props.isConnected === true ? menuLogged : menuLogOf}>
              <a href="/" className={'ant-dropdown-link'} ><IconButton hideBorder={true} iconType={"more"} /></a>
            </Dropdown>
          </div>
        </div>
      </ContainerInner>
    </Container>
  );
});

export default HeaderApp;

const Container = styled.div`
    width:100%;
    height:45px;
    background: rgb(66,24,187);
    background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
    margin:0;
    padding:0;
    `;

const ContainerInner = styled.div`
    display:block;
    height:44px;
    background:white;
    `;

const Logo = styled.img`
    flex: 0 auto;
    width: 34px;
    height:34px;
    `;

const Divider = styled.div<any>`
                  width: 1px;
                  height: 34px;
                  background: rgb(66,24,187);
                  background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
display: ${props => props.hide === true ? "none" : ""}
        `;

const UserName = styled(GradientText) <any>`
                          font-size: 18px;
                          margin: 0;
                          padding-top: 4px;
                          cursor:pointer;
  display: ${props => props.hide === true ? "none" : ""}
          `;

const Explorer = styled(GradientText) <any>`
                                font-size: 18px;
                                margin: 0;
                                padding-top: 4px;
                                cursor:pointer;
  display: ${props => props.hide === true ? "none" : ""}
            `;


const UserPicture = styled.img<{ hide?: any }>`
                                      width: 34px;
                                      height: 34px;
                                      border-radius: 30px;
  display: ${props => props.hide === true ? "none" : ""}
              cursor:pointer;
`;
import React from 'react';
import { withRouter } from 'react-router-dom';
import { Drawer, Checkbox, Select, Upload, Button, Icon, message } from 'antd';
import useDocumentFormUpload from '../Context/useDocumentFormUpload';
import styled from 'styled-components';

import { BaseUrl } from '../Context/store';

import { IconInput, TextArea } from './styled/input';

const { Option } = Select;
const DocumentFormUpload = withRouter((props: any) => {
    const { form, handleChange, onBlur, handleTargetChecked, handleChangeSelected } = useDocumentFormUpload();

    const isValidForm = () => {
        if (form.nameValid === true && form.descriptionValid) {
            return false;
        }
        return true;
    }
    const uploadProps = {
        name: 'file',
        multiple: true,
        action: `${BaseUrl}api/document`,
        data: {
            "name": form.name,
            "description": form.description,
            "public": form.public,
            "category": form.category
        },
        headers: {
            Authorization: `Bearer ${props.token}`,
        },
        onChange(info: any) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
                props.store.getMyDocument();
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    return (
        <Drawer
            title="Basic Drawer"
            placement="right"
            closable={props.closable}
            onClose={props.onClose}
            visible={props.isVisible}
            width={500}
        >
            <p>Veuillez remplir tous les champs pour procéder au téléchargement de votre document</p>
            <FormContainer width={props.width}>
                <ErrorText>{props.error}</ErrorText>
                <ErrorText>{form.nameError}</ErrorText>
                <IconInput value={form.name} placeholder={"Nom"} iconType={"user"} onChange={handleChange} name="name" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.descriptionError}</ErrorText>
                <TextArea value={form.description} placeholder={"Description"} onChange={handleChange} name="description" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <Select
                    defaultValue="administrative"
                    style={{ width: 200 }}
                    placeholder="Categories"
                    onChange={handleChangeSelected}
                >
                    <Option value="administrative">Administratif</Option>
                    <Option value="finance">Finance</Option>
                    <Option value="health">Santé</Option>
                    <Option value="entertainment">Divertissement</Option>
                    <Option value="business">Affaires</Option>
                </Select>
            </FormContainer>
            <Checkbox checked={form.public} name='public' onChange={handleTargetChecked}>Public</Checkbox >
            <Upload disabled={isValidForm()} {...uploadProps}>
                <Button>
                    <Icon type="upload" /> Ouvrir
                </Button>
            </Upload>

        </Drawer>
    )
});

export default DocumentFormUpload;

const FormContainer = styled.div<any>`
                            width: 400px;
                            border-bottom: 1px solid #335AF0;
                            margin-top: 20px;
                            margin-bottom:20px;
                            display:flex;
                            flex-direction:column;
                            align-content:space-between;
                        `;

const ErrorText = styled.p`
                            color: red;
`;

/*const ButtonSubmit = styled.button<any>`
border:none;
width: 400px;
height:36px;
border-radius: 30px;
cursor:pointer;
color:white;
text-align:center;
margin: auto;
margin-top: 30px;
line-height:36px;
background: rgb(84,144,228);
background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
`;*/
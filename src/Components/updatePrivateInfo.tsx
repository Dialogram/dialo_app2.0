import React from 'react';
import { Input, Form, Button } from 'antd';
import Styled from 'styled-components';

const UpdatePrivateInfo = (props: any) => {

    const handleSubmit = (e: any) => {
        e.preventDefault();
        props.form.validateFields((err: any, values: any) => {
            if (!err) {
                console.log('Received values of form: ', values);
                props.onSubmit(values);
            }
        });
    }
    const emailValidator = (rule: any, value: any, callback: any) => {
        const { form } = props;
        if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            callback("L'adresse e-mail est mal formée.");
        } else {
            callback();
        }
    };
    return (
        <Form onSubmit={handleSubmit}>
            <Form.Item>
                {props.form.getFieldDecorator('email', {
                    rules: [{ required: true, message: 'L\'adresse e-mail n\'est pas valide' },
                    {
                        validator: emailValidator,
                    }],
                    initialValue: props.data.email
                })(
                    <Input addonBefore="Email" />
                )}
            </Form.Item>
            <Form.Item>
                {props.form.getFieldDecorator('nickName', {
                    rules: [{ required: true, message: 'Ne peut être vide' }
                    ],
                    initialValue: props.data.nickName
                })(
                    <Input addonBefore="Surnom" />
                )}
            </Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
                Modifier
          </Button>
        </Form>
    );
}

export default Form.create<any>({ name: 'register' })(UpdatePrivateInfo);

const EditAction = Styled.p`
cursor:pointer;
color: rgba(66,24,187,1);
padding:0;
margin:0;

`;

import React, { useState, useEffect } from "react";
import "./App.css";

import { StoreConsumer } from "./Context";
import Wrapper from "./wrapper";
import Session from "./Models/Session";
/*
import socketIOClient from "socket.io-client";

const socket = socketIOClient("http://localhost:8085");
socket.on("FromAPI", (data: any) => console.log(data));

*/
const App = (props: { store: any }) => {
  const [loading, setLoading] = useState(true);

  const relogin = async (session: Session) => {
    await props.store.setSession(session);
    try {
      await props.store.getUser()
      setLoading(false);
    } catch (e) {
      setLoading(false);
      console.log(e);
    }
  }

  useEffect(() => {

    let token = localStorage.getItem("token") || undefined;
    let user = localStorage.getItem("userId") || undefined;
    let session = new Session({
      token: token,
      user: user
    });

    relogin(session);

    /*if (token !== undefined) {
      props.store.setSession(session).then(() => {
        props.store.getUser().catch((e: any) => {
          setLoading(false);
        }).then(() => {
          setLoading(false);
        })
      })
    } else {
      setLoading(false);
    }*/
  }, []);

  if (loading === true)
    return <div>Chargement.....loading</div>
  return <StoreConsumer>{store => <Wrapper store={store} />}</StoreConsumer>;
};

export default App;

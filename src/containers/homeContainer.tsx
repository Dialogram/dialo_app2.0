import React, { useEffect, useState } from 'react';
import Styled from 'styled-components';
import Document from '../Models/Document';
import { Icon } from 'antd';
import { withRouter } from "react-router-dom";
import DocumentCard from '../Components/document-card';

const HomeContainer = withRouter((props: any) => {

    const [range, setRange] = useState({ from: 0, to: 4 });
    useEffect(() => {
        props.store.getMyDocument();
    }, [])

    const nextDoc = () => {
        setRange({ from: range.from + 4, to: range.to + 4 })
    };
    const prevDoc = () => {
        setRange({ from: (range.from - 4) < 0 ? 0 : range.from - 4, to: (range.to - 4) < 4 ? 4 : range.to - 4 })
    }

    const goToDocument = (e: any, id: string) => {
        props.history.push(`/document/${id}`);
    }
    return (
        <Container width={props.width}>
            <MediumTitle>Derniers documents lus</MediumTitle>
            <Row width={props.width}>
                {range.from > 0 && <LeftArrow onClick={prevDoc}><Icon type="left" /></LeftArrow>}
                {props.store.userDocument.map((value: Document, index: number) => {

                    if (index >= range.from && index <= range.to) {
                        return (<CardContainer key={index} onClick={(e: any) => goToDocument(e, value.id)}><DocumentCard doc={value} token={props.store.session.token} /></CardContainer>)
                    } else {
                        return null;
                    }
                })}
                {(range.to < props.store.userDocument.length && props.store.userDocument.length % 4 !== 0) && <RightArrow onClick={nextDoc}><Icon type="right" /></RightArrow>}
            </Row>

        </Container >
    )
});

export default HomeContainer;

const RightArrow = Styled.div`
width: 40px;
height: 40px;
background: rgb(66,24,187);
background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
border-radius: 40px;
position: relative;
z-index: 4;
left: -13px;
padding: 13px;
color: white;
line-height: 14px;
cursor:pointer;
`;
const LeftArrow = Styled.div`
width: 40px;
height: 40px;
background: rgb(66,24,187);
background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
border-radius: 40px;
position: relative;
z-index: 4;
left:15px;
padding: 13px;
color: white;
line-height: 14px;
cursor:pointer;
`;

const MediumTitle = Styled.h3`
font-size:16px;
margin:0;
padding:0;
padding-left:10px;
color: #5B5B5B;
margin-bottom:10px;
margin-top:10px;
`;

const CardContainer = Styled.div<any>`
margin-left:20px;
margin-right:20px;
`;

const Row = Styled.div<any>`
width:${props => props.width < 768 ? '100%' : '1280px'};
display: flex;
flex-direction: ${props => props.width < 768 ? 'column' : 'row'};
justify-content: space-arround;
align-items: center;
`;
const Container = Styled.div<any>`
width: ${props => props.width > 768 ? '1280px' : '100%'};
margin: auto;
overflow:hidden;
`;
import React from 'react';
import Styled from 'styled-components';
import { Row, Col, notification } from 'antd';

import UpdatePublicInfo from '../Components/updatePublicInfo';
import UpdatePrivateInfo from '../Components/updatePrivateInfo';
import UpdatePassword from '../Components/updatePassword';
import User from '../Models/User';


const failNotif = () => {
    notification.error({
        message: 'Modification refusé',
        description:
            'Une erreur est survenue lors de votre modification. Verifiez que vous ayez rentrés des données correctes',
        duration: 2
    });
};

const successNotif = (desc: string) => {
    notification.success({
        message: 'Modification réussie',
        description:
            desc,
        duration: 2
    });
};


const AccountContainer = (props: any) => {
    //TODO ==> Continuer à split en component + passer les events en propsèèèèèèèèèèèèèèèèèè


    const handleSubmitPublic = (values: any) => {
        console.log({ profile: values })

        props.store.updatePublicInfo({ profile: values }).catch((e: any) => {
            failNotif();
            return Promise.reject(e);
        }).then((val: User) => {
            console.log("SUCCESS", val);
            successNotif('Votre profil a bien été modifié');
        })


    }
    const handleSubmitPrivate = (values: any) => {
        console.log("private info");
        props.store.updateAccountInfo(values).catch((e: any) => {
            failNotif();
            return Promise.reject(e);
        }).then((val: User) => {
            console.log("SUCCESS", val);
            successNotif('Un e-mail vous a été envoyé afin de confirmer votre changement');
        })
    }

    const handleSubmitPassword = (values: any) => {
        console.log("password info")
        props.store.updatePassword(values).catch((e: any) => {
            failNotif();
            return Promise.reject(e);
        }).then((val: User) => {
            console.log("SUCCESS", val);
            successNotif('Un e-mail vous a été envoyé afin de confirmer votre changement');
        })
    }

    return (
        <div>
            <Header>
                <Container width={props.width}>
                    <HeaderTitle>Gestion de compte</HeaderTitle>
                </Container >
            </Header>
            <Container width={props.width}>
                <RowWrapper>
                    <ColWrapper span={8} >
                        <SectionTitle>Information publiques</SectionTitle>
                        <SectionDesc>Vos informations personnelles sont affichés sur votre profil publique.</SectionDesc>
                    </ColWrapper>
                    <ColWrapper span={16} >
                        <UpdatePublicInfo onSubmit={handleSubmitPublic} data={props.store.user.profile} />
                    </ColWrapper>

                </RowWrapper>
                <RowWrapper>
                    <ColWrapper span={8} >
                        <SectionTitle>Identiants</SectionTitle>
                        <SectionDesc>Votre adresse e-mail est utilisé pour vous connecter, votre username est votre identité sur Dialogram.</SectionDesc>
                    </ColWrapper>
                    <ColWrapper span={16} >
                        <UpdatePrivateInfo onSubmit={handleSubmitPrivate} data={props.store.user} />
                    </ColWrapper>
                </RowWrapper>
                <RowWrapper>
                    <ColWrapper span={8} >
                        <SectionTitle>Password</SectionTitle>
                        <SectionDesc>Si vous demandez un changement de mot de passe une confirmation vous sera demandé par e-mail.</SectionDesc>
                    </ColWrapper>
                    <ColWrapper span={16} >
                        <UpdatePassword onSubmit={handleSubmitPassword} />
                    </ColWrapper>
                </RowWrapper>
            </Container >
        </div>

    )
};

export default AccountContainer;


const SectionTitle = Styled.h4`
color: rgba(66,24,187,1);
flex: 1;
`;

const SectionDesc = Styled.p`
flex: 1;
`;

const RowWrapper = Styled(Row)`
    border-bottom: 1px solid #e7ebf3;
    padding-bottom: 20px;
`;
const ColWrapper = Styled(Col)`
    display: flex;
    flex-direction: column;
    padding-right: 20px;
    padding-top: 20px;
`;

const HeaderTitle = Styled.h3`
margin:0;
padding:0;
line-height: 50px;
`;

const Header = Styled.div`
width: 100%;
height: 50px;
border-bottom: 1px solid #e7ebf3;
background: #FBF5FF ;
`;
const Container = Styled.div<any>`
width: ${props => props.width > 768 ? '1280px' : '100%'};
margin: auto;
overflow:hidden;
`;
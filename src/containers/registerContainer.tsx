import React from 'react';
import styled from 'styled-components';
import { IconInput } from '../Components/styled/input';
import { GradientText } from '../Components/styled/gradientText';
import useRegisterForm from '../Context/useRegisterForm';
import { withRouter, Link } from "react-router-dom";


const RegisterContainer = withRouter((props: any) => {
    const { handleChange, form, onBlur, setForm } = useRegisterForm();

    const submit = async (e: any) => {
        if (form.emailValid === true && form.passwordValid === true) {
            try {
                await props.store.createUser(form);
                props.history.push('/');
            } catch (err) {
                setForm({
                    ...form,
                    formError: err.message
                })
            }
        }
    }

    return (
        <Container width={props.width}>
            <Logo src={require('../assets/img/2020_logo_DIALOGRAM.png')} />
            <AppName>DIALOGRAM</AppName>
            <FormContainer width={props.width}>
                <ErrorText>{form.nameError}</ErrorText>
                <IconInput placeholder={"Nom"} iconType={"user"} onChange={handleChange} name="name" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.firstNameError}</ErrorText>
                <IconInput placeholder={"Prénom"} iconType={"crown"} onChange={handleChange} name="firstName" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.nickNameError}</ErrorText>
                <IconInput placeholder={"Pseudonyme"} iconType={"skin"} onChange={handleChange} name="nickName" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.formError}</ErrorText>
                <ErrorText>{form.emailError}</ErrorText>
                <IconInput placeholder={"E-mail"} iconType={"mail"} onChange={handleChange} name="email" onBlur={onBlur} />
            </FormContainer>
            <FormContainer width={props.width}>
                <ErrorText>{form.passwordError}</ErrorText>
                <IconInput type={'password'} placeholder={"Mot de passe"} iconType={"lock"} onChange={handleChange} name="password" onBlur={onBlur} />
            </FormContainer>
            <Button onClick={submit}>INSCRIPTION</Button>
            <ForgetPasswordLink><Link to={'/login'}>J'ai déjà un compte</Link></ForgetPasswordLink>
        </Container>
    )
})

export default RegisterContainer;



const Container = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '1280px'};
    margin: auto;
    display: flex;
    flex-direction:column;
    justify-content:center;
`;

const Logo = styled.img<any>`
    width: 150px;
    height:150px;
    margin: auto;
    margin-top: 50px;
`;

const AppName = styled(GradientText)`
font-size:24px;
text-align:center;
`;

const FormContainer = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '400px'};
    margin: auto;
    border-bottom: 1px solid #335AF0;
    margin-top: 50px;
    display:flex;
    flex-direction:column;
    align-content:space-between;
`;

const Button = styled.button<any>`
border:none;
width: 400px;
height:36px;
border-radius: 30px;
cursor:pointer;
color:white;
text-align:center;
margin: auto;
margin-top: 30px;
line-height:36px;
background: rgb(84,144,228);
background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
`;

const ForgetPasswordLink = styled.a<any>`
background: rgb(84,144,228);
background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
-webkit-background-clip: text;
-webkit-text-fill-color: transparent;
text-align:center;
margin-top:30px;
`;

const ErrorText = styled.p`
    color: red;
`;
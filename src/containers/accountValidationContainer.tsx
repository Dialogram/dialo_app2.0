import React, { useEffect, useState } from 'react';
import { withRouter } from "react-router-dom";
import { Row, Col, notification, Result } from 'antd';
import styled from 'styled-components';

const AccountValidationContainer = withRouter((props: any) => {
  const [result, setResult] = useState();

  useEffect(() => {
    // props.store.getMyDocument();
    console.log(props.match.params.token)
    props.store.confirmAccount(props.match.params.token).catch((e: any) => {
      setResult(false);
      return Promise.reject();
    }).then((val: any) => {
      setResult(true);
    })
  }, [])

  const success = () => {
    return (<Result
      status="success"
      title="Merci d'avoir confirmé votre compte et bienvenue"
      subTitle="Vous pouvez reprendre vote navigation"
    />)
  }

  const error = () => {
    return (<Result
      status="error"
      title="Nous ne sommes pas en mesure de valider votre compte"
      subTitle="Avez vous bien utilisé le mail donné dans l'email?"
      extra={
        <a onClick={e => resendToken()}>Renvoyer une confirmation</a>
      }
    />)
  }

  const resendToken = () => {
    props.store.sendConfirmAccount().catch((e: any) => {
      failNotif();
      return Promise.reject();
    }).then((e: any) => {
      console.log("token send");
      successNotif("Un e-mail avec une nouvelle demande de confirmation vous a été envoyé.")
    })
  }

  const failNotif = () => {
    notification.error({
      message: 'Erreur',
      description:
        'Une erreur est survenue lors du rafraichissement de la confirmation de compte',
      duration: 2
    });
  };

  const successNotif = (desc: string) => {
    notification.success({
      message: 'Confirmation demandé !',
      description:
        desc,
      duration: 2
    });
  };

  return (
    <Container>
      {result === true && success()}
      {result === false && error()}
    </Container>

  )
});

export default AccountValidationContainer;


const Container = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '1280px'};
    margin: auto;
    display: flex;
    flex-direction:column;
    justify-content:center;
`;
import React, { useEffect, useState } from 'react';
import { withRouter, Link } from "react-router-dom";
import { Row, Col, notification, Result } from 'antd';
import styled from 'styled-components';
import { IconInput } from '../Components/styled/input';

const PasswordForgetSendContainer = withRouter((props: any) => {
  const [isSend, setIsSend] = useState(false);
  const [result, setResult] = useState();
  const [password, setPassword] = useState();
  const [confirm, setConfirm] = useState();
  const [passwordError, setPasswordError] = useState('');
  const [confirmError, setConfirmError] = useState('');
  /*useEffect(() => {
    // props.store.getMyDocument();
    console.log(props.match.params.token)
    props.store.confirmAccount(props.match.params.token).catch((e: any) => {
      setResult(false);
      return Promise.reject();
    }).then((val: any) => {
      setResult(true);
    })
  }, [])*/

  const success = () => {
    return (<Result
      status="success"
      title="Votre mot de passe a été mis à jour"
      subTitle="Vous pouvez reprendre vote navigation"
      extra={
        <Link to={'/login'}>Connectez vous</ Link>
      }
    />)
  }

  const error = () => {
    return (<Result
      status="error"
      title="Nous ne sommes pas en mesure de changer votre mot de passe"
      subTitle="Avez vous bien utilisé l'adresse donné dans l'email?"

    />)
  }

  /*const resendToken = () => {
            props.store.sendConfirmAccount().catch((e: any) => {
              failNotif();
              return Promise.reject();
            }).then((e: any) => {
              console.log("token send");
              successNotif("Un e-mail avec une nouvelle demande de confirmation vous a été envoyé.")
            })
          }*/

  const failNotif = () => {
    notification.error({
      message: 'Erreur',
      description:
        'Une erreur est survenue lors du changement de mot de passe.',
      duration: 2
    });
  };

  const successNotif = (desc: string) => {
    notification.success({
      message: 'Changement de mot de passe effectué !',
      description:
        desc,
      duration: 2
    });
  };

  const onBlurPassword = (e: any) => {

  }

  const onBluronfirmation = (e: any) => {

  }
  const onChangePassword = (e: any) => {
    setPasswordError('');
    setPassword(e.target.value)
  }
  const onChangeConfirm = (e: any) => {
    setConfirmError('');
    setConfirm(e.target.value)
  }

  const resetPassword = (e: any) => {

    console.log("reset ?")
    if (password && password.length < 8) {
      setPasswordError("Le mot de passe doit faire 8 caracètres minimum.");
      return;
    }
    if (confirm !== password) {
      setConfirmError("Les mots de passe ne sont pas identiques");
      return;
    }

    props.store.resetPasswordWithToken(password, confirm, props.match.params.token).catch((err: any) => {
      setIsSend(true);
      console.log("passowrd reset fail")
      setResult(false);
      return Promise.reject();
    }).then((val: any) => {
      console.log("password reset");
      setIsSend(true);
      setResult(true);
    })
    console.log("reset password")
  }

  if (isSend === false) {
    return (
      <Container>
        <FormContainer width={props.width}>
          <ErrorText>{passwordError}</ErrorText>
          <IconInput type={"password"} id={"passwordR"} placeholder={"Mot de passe"} iconType={"lock"} onChange={onChangePassword} name="password" onBlur={onBlurPassword} />
        </FormContainer>
        <FormContainer width={props.width}>
          <ErrorText>{confirmError}</ErrorText>
          <IconInput type={"password"} id={"passwordRC"} placeholder={"Confimation"} iconType={"lock"} onChange={onChangeConfirm} name="confirmation" onBlur={onBluronfirmation} />

        </FormContainer>
        <Button id={"submit"} onClick={resetPassword}>ENVOYER</Button>
      </Container>

    )
  } else {
    return (
      <Container>
        {result === true && success()}
        {result === false && error()}
      </Container>
    )
  }

});

export default PasswordForgetSendContainer;

const Button = styled.button<any>`
          border:none;
          width: 400px;
          height:36px;
          border-radius: 30px;
          cursor:pointer;
          color:white;
          text-align:center;
          margin: auto;
          margin-top: 30px;
          line-height:36px;
          background: rgb(84,144,228);
          background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
          `;


const ErrorText = styled.p`
              color: red;
          `;

const Container = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '1280px'};
              margin: auto;
              display: flex;
              flex-direction:column;
              justify-content:center;
          `;

const FormContainer = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '400px'};
                margin: auto;
                border-bottom: 1px solid #335AF0;
                margin-top: 50px;
                display:flex;
                flex-direction:column;
                align-content:space-between;
`;
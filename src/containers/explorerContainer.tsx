import React, { useEffect, useState } from 'react';
import Styled from 'styled-components';
import Document from '../Models/Document';
import { Icon } from 'antd';
import { withRouter } from "react-router-dom";
import DocumentCard from '../Components/document-card';
import DocumentSlider from '../Components/documentSlider';
import MyDocumentSlider from '../Components/myDocumentSlider';
import { StoreConsumer } from '../Context';
const ExplorerContainer = withRouter((props: any) => {

    const [range, setRange] = useState({ from: 0, to: 4 });
    const [isMine, setIsMine] = useState(false);

    return (
        <Container width={props.width}>
            <button onClick={(e: any) => { setIsMine(!isMine) }}>Mes doc</button>
            <StoreConsumer>
                {store => <MyDocumentSlider store={store} title={"Mes documents"} category={'health'} token={props.store.session.token}></MyDocumentSlider>}
            </StoreConsumer>
            <StoreConsumer>
                {store => <DocumentSlider store={store} title={"Santé"} category={'health'} token={props.store.session.token}></DocumentSlider>}
            </StoreConsumer>
            <StoreConsumer>
                {store => <DocumentSlider store={store} title={"Finance"} category={'finance'} token={props.store.session.token}></DocumentSlider>}
            </StoreConsumer>
            <StoreConsumer>
                {store => <DocumentSlider store={store} title={"Administratif"} category={'administrative '} token={props.store.session.token}></DocumentSlider>}
            </StoreConsumer>
            <StoreConsumer>
                {store => <DocumentSlider store={store} title={"Divertissement"} category={'entertainment'} token={props.store.session.token}></DocumentSlider>}
            </StoreConsumer>
            <StoreConsumer>
                {store => <DocumentSlider store={store} title={"Entreprise"} category={'business'} token={props.store.session.token}></DocumentSlider>}
            </StoreConsumer>
        </Container >
    )
});

export default ExplorerContainer;

const RightArrow = Styled.div`
width: 40px;
height: 40px;
background: rgb(66,24,187);
background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
border-radius: 40px;
position: relative;
z-index: 4;
left: -13px;
padding: 13px;
color: white;
line-height: 14px;
cursor:pointer;
`;
const LeftArrow = Styled.div`
width: 40px;
height: 40px;
background: rgb(66,24,187);
background: linear-gradient(168deg, rgba(66,24,187,1) 43%, rgba(85,142,227,1) 95%);
border-radius: 40px;
position: relative;
z-index: 4;
left:15px;
padding: 13px;
color: white;
line-height: 14px;
cursor:pointer;
`;

const MediumTitle = Styled.h3`
font-size:16px;
margin:0;
padding:0;
padding-left:10px;
color: #5B5B5B;
margin-bottom:10px;
margin-top:10px;
`;

const CardContainer = Styled.div<any>`
margin-left:20px;
margin-right:20px;
`;

const Row = Styled.div<any>`
width:${props => props.width < 768 ? '100%' : '1280px'};
display: flex;
flex-direction: ${props => props.width < 768 ? 'column' : 'row'};
justify-content: space-arround;
align-items: center;
`;
const Container = Styled.div<any>`
width: ${props => props.width > 768 ? '1280px' : '100%'};
margin: auto;
overflow:hidden;
`;
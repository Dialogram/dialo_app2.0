import React, { useState } from 'react';
import styled from 'styled-components';
import { IconInput } from '../Components/styled/input';
import { GradientText } from '../Components/styled/gradientText';
import useLoginForm from '../Context/useLoginForm';
import { withRouter, Link } from "react-router-dom";
import { Modal, Drawer, notification } from 'antd';


const SigninContainer = withRouter((props: any) => {
    const { handleChangeEmail, form, handleChangePassword, onBlurEmail, onBlurPassword, setForm } = useLoginForm();
    const [forget, setForget] = useState(false);
    const [email, setEmail] = useState();
    const [emailError, setEmailError] = useState('');
    const submit = async (e: any) => {
        if (form.emailValid === true && form.passwordValid === true) {
            try {
                await props.store.createSession(form);
                props.history.push('/');
            } catch (err) {
                setForm({
                    ...form,
                    formError: err.message
                })
            }
        }
    }

    const handlePasswordForgetInput = (e: any) => {
        console.log("value", e.target.value)
        setEmail(e.target.value)
    }

    const onPasswordForget = (e: any) => {
        setForget(true);
    }
    const onEmailBlur = (e: any) => {

    }
    const resetPassword = (e: any) => {
        e.preventDefault();
        if (email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
            console.log("send reset password")
            props.store.forgetPassword(email).catch((err: any) => {
                failNotif();
                return Promise.reject();
            }).then((val: boolean) => {
                if (val === true) {
                    successNotif("Un e-mail vous a été envoyé.")
                }
            })
        } else {
            setEmailError("L'adresse e-mail est mal formée.")
        }
    }

    const failNotif = () => {
        notification.error({
            message: 'Erreur',
            description:
                'Une erreur est survenue lors du changement de mot de passe.',
            duration: 2
        });
    };

    const successNotif = (desc: string) => {
        notification.success({
            message: 'Changement de mot de passe effectué !',
            description:
                desc,
            duration: 2
        });
    };


    if (forget === true) {
        return (
            <Container width={props.width}>
                <Logo src={require('../assets/img/2020_logo_DIALOGRAM.png')} />
                <AppName>DIALOGRAM</AppName>

                <FormContainer width={props.width}>
                    <ErrorText>{emailError}</ErrorText>
                    <IconInput id={"email"} placeholder={"E-mail"} iconType={"mail"} onChange={handlePasswordForgetInput} name="email" onBlur={onEmailBlur} />
                    <Button id={"submit"} onClick={resetPassword}>ENVOYER</Button>
                </FormContainer>
            </Container>
        )
    } else {
        return (
            <Container width={props.width}>

                <Logo src={require('../assets/img/2020_logo_DIALOGRAM.png')} />
                <AppName>DIALOGRAM</AppName>

                <FormContainer width={props.width}>
                    <ErrorText>{form.formError}</ErrorText>
                    <ErrorText>{form.emailError}</ErrorText>
                    <IconInput id={"email"} placeholder={"E-mail"} iconType={"mail"} onChange={handleChangeEmail} name="email" onBlur={onBlurEmail} />
                </FormContainer>
                <FormContainer width={props.width}>
                    <ErrorText>{form.passwordError}</ErrorText>
                    <IconInput id={"password"} type={"password"} placeholder={"Mot de passe"} iconType={"lock"} onChange={handleChangePassword} name="password" onBlur={onBlurPassword} />
                </FormContainer>
                <Button id={"submit"} onClick={submit}>CONNEXION</Button>
                <ForgetPasswordLink onClick={(e: any) => onPasswordForget(e)}> Mot de pass oublié ?</ForgetPasswordLink>
                <LinkRegister to={'/register'}> <Button>INSCRIPTION</Button></LinkRegister>
            </Container>
        )
    }
})

export default SigninContainer;



const Container = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '1280px'};
    margin: auto;
    display: flex;
    flex-direction:column;
    justify-content:center;
`;

const Logo = styled.img<any>`
    width: 150px;
    height:150px;
    margin: auto;
    margin-top: 50px;
`;

const AppName = styled(GradientText)`
font-size:24px;
text-align:center;
`;

const FormContainer = styled.div<any>`
    width: ${props => props.width < 768 ? '100%' : '400px'};
    margin: auto;
    border-bottom: 1px solid #335AF0;
    margin-top: 50px;
    display:flex;
    flex-direction:column;
    align-content:space-between;
`;

const Button = styled.button<any>`
border:none;
width: 400px;
height:36px;
border-radius: 30px;
cursor:pointer;
color:white;
text-align:center;
margin: auto;
margin-top: 30px;
line-height:36px;
background: rgb(84,144,228);
background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
`;

const LinkRegister = styled(Link)`
width: 400px;
height:36px;
margin: auto;
`;

const ForgetPasswordLink = styled.a<any>`
background: rgb(84,144,228);
background: linear-gradient(168deg, rgba(84,144,228,1) 0%, rgba(49,87,240,1) 100%);
-webkit-background-clip: text;
-webkit-text-fill-color: transparent;
text-align:center;
margin-top:30px;
`;

const ErrorText = styled.p`
    color: red;
`;
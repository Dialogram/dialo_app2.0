import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { withRouter } from "react-router-dom";
import { Document, Page, pdfjs } from 'react-pdf';
import { Icon, Popover, Pagination } from 'antd';
import useTraductionContext from '../Context/useTraductionContext';
import { Rnd } from 'react-rnd';
import Traduction from '../Models/Traduction';
import { FilledIconButton } from '../Components/styled/button';
import DocumentFormUpdate from '../Components/documentFormUpdate';
import Doc from '../Models/Document';
import VideoFormUpload from '../Components/videoFormUpload';
import Video from '../Models/Video';
import VideoList from '../Components/videoList';
import Zone from '../Models/Zone';
import { v4 } from 'uuid';
import DisplayHls from '../Components/displayHls';
import RolePanel from '../Components/rolePanel';
import { StoreConsumer } from '../Context';
import User from '../Models/User';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;



const DocumentContainer = withRouter((props: any) => {
    const [pageNumber, setPageNumber] = useState(1);
    const [actualPage, setActualPage] = useState(1);
    const [document, setDocument] = useState();
    const [toggle, setToogle] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [drawerVisible, setDrawerVisible] = useState(false);
    const [drawerVisibleUser, setDrawerVisibleUser] = useState(false);
    const [videoDrawerVisible, setVideoDrawerVisible] = useState(false);
    const [videoListDrawerVisible, setVideoListDrawerVisible] = useState(false);
    const [lastZoneReceive, setLastZoneReceive] = useState();
    const {
        traductions,
        refreshZoneArray,
        refreshZone,
        getZone,
        deleteZone,
        pushZone,
        selectZone,
        dragStop,
        resize,
        toPixel,
        toPercent,
    } = useTraductionContext();
    const [error, setError] = useState("");
    const [timeout, setMyTimeOut] = useState();
    const [hls, setHls] = useState(undefined);
    const urlDocId = props.match.params.id;
    const [owner, setOwner] = useState();

    useEffect(() => {
        console.log("RENDERIN")
        props.store.getSingleDocument(urlDocId).catch((e: any) => {
            console.log(e)
        }).then((doc: Doc) => {
            console.log("doc", doc);
            props.store.socket.emit("join-room", { id: doc.id, userId: props.store.user.id });
            setDocument(doc);
            getTraduction();
            getUserVideo();
            getUserOwner(doc);
            props.store.socket.on('new_zone', (data: any) => {
                if (data.emitBy === props.store.user.id)
                    return;
                getTraduction();

            })
        })
        return function cleanup() {
            props.store.socket.removeListener('new_zone', () => { console.log("new_zone_removed") })
        };
    }, [])

    const getUserOwner = (document: Doc) => {
        if (document.idOwner === props.store.user.id) {
            console.log("Owner =  me = ", props.store.user)
            setOwner(props.store.user);
        } else {
            props.store.getUserById(document.idOwner).catch((e: any) => {
                console.log("Owner not found");
            }).then((val: User) => {
                console.log("Owner = ", val)
                setOwner(val);
            })
        }
    }

    const getTraduction = () => {
        props.store.getMasterTraduction(urlDocId).catch((e: any) => {
            console.log(e)
        }).then((val: Traduction) => {
            console.log("traducon", val)
            refreshZoneArray(val.zones);
        })
    }


    const onDocumentLoadSuccess = ({ numPages }: { numPages: number }) => {
        setPageNumber(numPages);
        console.log("PAGE number", numPages)
    };

    const getUserVideo = async () => {
        await props.store.getUserVideo();
    }

    if (!document) {
        return (<div>Loading..</div>)
    }

    const content = (
        <div>
            <p onClick={e => setVideoListDrawerVisible(true)}>Lier à une vidéo</p>
            <p onClick={(e: any) => { onDeleteZone(document.id, document.idMasterTranslation, traductions.selectedZone); e.preventDefault() }} >Supprimer</p>
        </div>
    );

    const uploadVideoSuccess = (e: any) => {
        setTimeout(() => {
            props.store.getUserVideo().then((val: Video[]) => {
                console.log(val);
            })
        }, 700)

    }

    const saveZoneOnDragStop = (id: string) => {
        clearTimeout(timeout);
        setMyTimeOut(setTimeout(() => {

            let z = getZone(id);
            if (z !== undefined && z.state !== "TEMP") {
                props.store.updateZone(document.id, document.idMasterTranslation, id, z as Zone).then(() => {
                    getTraduction();
                })

            }

        }, 300))
    }

    const onDragStop = (e: any, d: { x: number, y: number }, id: string, el: Zone) => {
        if (toPixel(1280, el.x) === d.x && toPixel(1810, el.y) === d.y)
            return;
        if (editMode === true) {
            dragStop(id, d.x, d.y);
            saveZoneOnDragStop(id);
        }
        selectZone(el._id as string)
        e.preventDefault();
        e.stopPropagation();
    }

    const onResize = (e: any, direction: any, ref: any, delta: any, position: { x: number, y: number }, id: string, zone: Zone) => {
        if (editMode === true) {
            resize(ref, position, id);
        }
        e.preventDefault();
        e.stopPropagation();
        selectZone(zone._id as string)
    }

    const onResizeStop = (docId: string, tradId: string, zoneId: string) => {
        let z = getZone(zoneId as string);
        if (z !== undefined) {
            props.store.updateZone(docId, tradId, zoneId, z as Zone).then(() => {
                getTraduction();
            })
        }
        selectZone(zoneId);
    }

    const updateDoc = (id: string, body: any) => {
        props.store.updateDocument(id, body).then(
            (doc: Doc) => {
                setDocument(doc);
                setDrawerVisible(false);
            }, (error: { error: string }) => {
                setError(error.error)
            })
    }

    const uploadVideo = (id: string, body: any) => {
        setVideoDrawerVisible(false);
    }

    const linkVideotoZone = (idVideo: string) => {
        setVideoListDrawerVisible(false);
        const z = getZone(traductions.selectedZone);
        if (z !== undefined) {
            const id = z._id;
            z.idVideo = idVideo;
            props.store.createZone(document.id, document.idMasterTranslation, z).then((z: Zone) => {
                deleteZone(id as string);
                getTraduction();
            })

        }
    }

    const onVideoDeleteConfirm = (idApiVideo: string) => {
        props.store.deleteVideo(idApiVideo);
    }

    const onDeleteDocument = (id: string) => {
        props.store.deleteDocument(id);
    }

    const onDeleteZone = (idDocument: string, idTraduction: string, idZone: string) => {
        props.store.deleteZone(idDocument, idTraduction, idZone)
        deleteZone(idZone as string);
    }

    const addZone = () => {
        const z = new Zone({ _id: v4(), width: toPercent(1280, 200), height: toPercent(1810, 200), x: toPercent(1280, 200), y: toPercent(1810, 200), page: actualPage, state: "TEMP" });
        pushZone(z);
    }

    const onSelectZone = (id: string) => {
        console.log("select zone");
        console.log("videoList", props.store.userVideo)
        selectZone(id);
        const z = getZone(id);
        if (z) {
            console.log("z", z.idVideo);
            const video = props.store.userVideo.find((element: Video) => {
                return element.id === z.idVideo;
            })
            if (video) {
                console.log("t", video.assets.hls, video);
                setHls(video.assets.hls)
            }
        }
    }
    const refreshDoc = (doc: Doc) => {
        console.log("REFRESH DOC", doc);
        // setDocument(doc);
        props.store.getSingleDocument(urlDocId).catch((e: any) => {
            console.log(e)
        }).then((doc: Doc) => {
            console.log("doc", doc);
            setDocument(doc);
        })
    }
    const closeVideoPlayer = () => {
        setHls(undefined);
    }

    const fixCors = () => {
        if (document) {
            const splitted = document.link.split('localhost');
            return `${splitted[0]}/localhost:8085${splitted[1]}?accessToken=${props.store.session.token}`
            //file={props.doc.link + '?accessToken=' + props.token}>
        }
        return '';
    }

    const goToPrevPage = () =>
        setActualPage(actualPage !== 1 ? actualPage - 1 : actualPage);
    //this.setState(state => ({ pageNumber: state.pageNumber !== 1 ? state.pageNumber - 1 : state.pageNumber }));

    const goToNextPage = () =>
        setActualPage(actualPage !== pageNumber ? actualPage + 1 : actualPage);
    //this.setState(state => ({ pageNumber: state.pageNumber !== this.state.numPages ? state.pageNumber + 1 : state.pageNumber }));


    return (
        <Container>
            <StoreConsumer>
                {store => <RolePanel store={store} document={document} refreshDoc={refreshDoc} closable={false} onClose={(e: any) => { setDrawerVisibleUser(false) }} isVisible={drawerVisibleUser} />}
            </StoreConsumer>
            <DisplayHls url={hls} closeHls={closeVideoPlayer}></DisplayHls>
            <StoreConsumer>
                {store => <DocumentFormUpdate refreshDoc={refreshDoc} store={store} deleteDocument={onDeleteDocument} error={error} onSubmit={updateDoc} document={document} width={props.width} closable={false} onClose={(e: any) => { setDrawerVisible(false) }} isVisible={drawerVisible}></DocumentFormUpdate>}
            </StoreConsumer>
            <VideoFormUpload uploadSuccess={(e: any) => { uploadVideoSuccess(e) }} token={props.store.session.token} error={error} onSubmit={uploadVideo} width={props.width} closable={false} onClose={(e: any) => { setVideoDrawerVisible(false) }} isVisible={videoDrawerVisible}></VideoFormUpload>
            <VideoList selectedVideo={linkVideotoZone} addVideo={(e: any) => {
                setVideoDrawerVisible(true);
            }}
                videos={props.store.userVideo}
                width={props.width}
                closable={false}
                onClose={(e: any) => { setVideoListDrawerVisible(false) }}
                isVisible={videoListDrawerVisible}
                onVideoDeleteConfirm={onVideoDeleteConfirm}
            >
            </VideoList>

            <ToolContainer width={props.width} toggle={toggle} hide={!editMode}>
                <ToolContainerElement>
                    <ToolIcon type={toggle === false ? "right" : "left"} onClick={(e: any) => { setToogle(!toggle) }} />
                </ToolContainerElement>
                <ToolContainerElement onClick={(e: any) => { addZone(); e.preventDefault() }}>
                    <ToolIcon id={"add-zone"} type="plus" /> <ToolName hide={!toggle}>Ajouter une zone</ToolName>
                </ToolContainerElement>
                <ToolContainerElement onClick={(e: any) => {
                    if (traductions.selectedZone !== "0") setVideoListDrawerVisible(true)
                }}>
                    <ToolIcon type="link" /> <ToolName hide={!toggle}>Lié une vidéo</ToolName>
                </ToolContainerElement>

                <ToolContainerElement onClick={(e: any) => { onDeleteZone(document.id, document.idMasterTranslation, traductions.selectedZone); e.preventDefault() }}>
                    <ToolIcon type="delete" /> <ToolName hide={!toggle}>Effacer la zone</ToolName>
                </ToolContainerElement>
            </ToolContainer>
            <ButtonTopContainer width={props.width}>
                <FilledIconButton id={"create-translate"} onClick={() => setEditMode(!editMode)} iconType="plus" label="Créé une traduction" type="button" ></FilledIconButton>
                <FilledIconButton onClick={() => setVideoDrawerVisible(true)} iconType="plus" label="Ajouter une vidéo" type="button" ></FilledIconButton>
            </ButtonTopContainer>
            <Paginate>
                <PaginateContainer>
                    <Left type="left" onClick={goToPrevPage} />

                    <PageInfos >Page {actualPage} sur {pageNumber}</PageInfos>
                    <Right type="right" onClick={goToNextPage} />
                </PaginateContainer>

            </Paginate>
            <DocContainer width={props.width}>

                <DocContainerHeader>
                    <DocContainerHeaderRightContent>
                        <UserPicture src={owner && owner.profile.profilePicture.url} />
                        <Doctitle >{document.name}</Doctitle>
                    </DocContainerHeaderRightContent>

                    {document.idOwner === props.store.user.id && <DocContainerHeaderRightContent>
                        <OverrideIcon type="edit" onClick={(e: any) => { setDrawerVisible(true) }} />
                        <OverrideIcon type="share-alt" />
                    </DocContainerHeaderRightContent>}
                </DocContainerHeader>

                <Document
                    onLoadSuccess={onDocumentLoadSuccess}
                    onLoadError={console.error}
                    file={document.link + '?accessToken=' + props.store.session.token}>
                    <Page width={1280} pageNumber={actualPage} />

                    {traductions.zones && Array.from(
                        traductions.zones,
                        (el, index) => ((el && el.page === actualPage) &&
                            <Popover overlayStyle={editMode === false ? { display: "none" } : {}} key={index} content={editMode === true ? content : null} trigger="click">
                                <Rnd
                                    onClick={(e: any) => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        onSelectZone(el._id as string);
                                    }}

                                    bounds={'parent'}
                                    style={{
                                        zIndex: 2,
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        border: 'solid 1px black',
                                        background: traductions.selectedZone === el._id ? 'rgba(0, 0, 255, 0.2)' : 'rgba(0, 255, 0, 0.2)',
                                    }
                                    }

                                    size={{
                                        width: toPixel(1280, el.width),
                                        height: toPixel(1810, el.height),
                                    }}
                                    position={{
                                        x: toPixel(1280, el.x),
                                        y: toPixel(1810, el.y),
                                    }}
                                    onDragStop={(e: any, d: any) => {
                                        onDragStop(e, d, el._id as string, el);
                                    }}
                                    onResize={(e: any, direction: any, ref: any, delta: any, position: any) => {
                                        onResize(e, direction, ref, delta, position, el._id as string, el);
                                    }}
                                    onResizeStop={() => {
                                        onResizeStop(document.id, document.idMasterTranslation, el._id as string);
                                    }}
                                    default={{
                                        x: toPixel(1280, el.x),
                                        y: toPixel(1810, el.y),
                                        width: toPixel(1280, el.width),
                                        height: toPixel(1810, el.height),
                                    }}>

                                </Rnd>
                            </Popover>))
                    }
                </Document>
            </DocContainer>
        </Container >
    )

});
const PageInfos = styled.p`
display: inline-block;
`;

const Left = styled(Icon)`
flex: 1;
`;

const Right = styled(Icon)`
flex: 1;
`;
const PaginateContainer = styled.div`
height: 20px;
flex:0 0 400px;
justify-content: center;
flex-direction:row;
display: flex;
`;

const Paginate = styled.div`
flex: 0 0 100%;
display: flex;
justify-content: center;
`;

const ButtonTopContainer = styled.div<any>`
width: ${props => props.width < 768 ? '100%' : '1280px'};
display: flex;
justify-content: flex-end;
margin: auto;
margin-top: 20px;
`;

const Doctitle = styled.p<any>`
  font-size: 18px;
  margin: 0;
  padding-top: 4px;
  margin-left: 10px;
`;

const UserPicture = styled.img<{ hide?: any }>`
  width: 34px;
  height: 34px;
  border-radius: 30px;
  display: ${props => props.hide === true ? "none" : ""}
  cursor:pointer;
`;

const DocContainerHeaderRightContent = styled.div`
display:flex;
display-direction:row;
justify-content: flex-end;
align-items: center;
`;

const OverrideIcon = styled(Icon)`
margin-right:20px;
font-size:18px;
color: #4218BB;
cursor: pointer;
`;

const DocContainerHeader = styled.div`
width:100%;
height:40px;
border-bottom: 1px solid black;
box-shadow: 0px -1px 4px rgba(0, 0, 0, 0.25);
display:flex;
display-direction:row;
justify-content: space-between;
align-items: center;
`;

const ToolIcon = styled<any>(Icon)`
font-size: 45px;
margin:0;
margin-left: 12px;
cursor: pointer;
`;

const ToolContainerElement = styled.div<any>`
width: 100%;
height: 60px;
overflow:hidden;
display:flex;
flex-direction:row;
justify-content: space-between;
align-items: center;
color:#4218BB;
`;

const ToolName = styled.p<any>`
${props => props.hide && 'display:none'}
margin-right:10px;
margin: 0;
`;
const ToolContainer = styled.div<any>`
width: ${props => props.width > 768 ? props.toggle ? '150px' : '80px' : '263px'};
height: ${props => props.width > 768 ? '263px' : '80px'};
background: white;
border: 1px solid #4218BB;
position:fixed;
display: ${props => props.hide ? 'none' : 'flex'}
flex-direction: ${props => props.width < 768 ? 'row' : 'column'}
top: ${props => props.width > 768 ? '100px' : '50px'};
left:0;
z-index:200;
`;

const Container = styled.div`
width: 100%;
`;

const DocContainer = styled.div<any>`
width: ${props => props.width < 768 ? '100%' : '1280px'};
min-height:30px;
margin:auto;
margin-top:  ${props => props.width < 768 ? '85px' : '15px'};
box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
`;

export default DocumentContainer;

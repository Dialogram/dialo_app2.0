import React from "react";

import { BrowserRouter as Router, Route } from "react-router-dom";

import Store from "./Models/store";
import HeaderApp from "./Components/header";
import HomeView from "./Views/HomeView";
import SigninView from "./Views/SinginView";
import AdminView from "./Views/AdminView";
import DocumentView from './Views/DocumentView';

import PrivateRoute from "./Components/privateRoute";
import RegisterView from "./Views/RegisterView";
import AccountView from './Views/AccountView';
import ExplorerView from './Views/ExplorerView';
import { StoreConsumer } from "./Context";
import AccountValidationView from './Views/AccountValidationView';
import PasswordForgetView from './Views/PasswordForgetView';

interface Props {
  store: Store;
}

export default function Wrapper(props: Props) {

  return (
    <div>
      <Router>
        {props.store.isConnected &&
          <StoreConsumer>
            {store => <HeaderApp isConnected={props.store.isConnected} logout={props.store.logout} store={store} />}
          </StoreConsumer>
        }

        <div>
          <PrivateRoute
            exact path="/"
            component={ExplorerView}
            store={props.store}
          />
          <PrivateRoute
            path="/document/:id"
            component={DocumentView}
            store={props.store}
          />
          <PrivateRoute
            path="/account/confirm/:token"
            component={AccountValidationView}
            store={props.store}
          />
          <PrivateRoute
            path="/explorer"
            component={ExplorerView}
            store={props.store}
          />
          <PrivateRoute
            exact
            path="/account"
            component={AccountView}
            store={props.store}
          />
          <Route path="/login" component={SigninView} />
          <Route path="/register" component={RegisterView} />
          <Route path="/password/forget/:token" component={PasswordForgetView} />
          <PrivateRoute
            path="/admin"
            component={AdminView}
            store={props.store}
          />

        </div>
      </Router>
    </div>
  );
}

import React from "react";
import useWindowDimensions from "../Context/useWindowDimensions";

import { StoreConsumer } from "../Context";
import AccountContainer from "../containers/accountContainer";

export default function AccountView(props: any) {
    const { width } = useWindowDimensions();
    return <StoreConsumer>
        {store => <AccountContainer width={width} store={store} />}
    </StoreConsumer>;
}

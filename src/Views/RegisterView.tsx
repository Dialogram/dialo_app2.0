import React from "react";
import { StoreConsumer } from "../Context";
import RegisterContainer from "../containers/registerContainer";
import useWindowDimensions from "../Context/useWindowDimensions";

export default function RegisterView(props: any) {
  const { width } = useWindowDimensions();
  return <StoreConsumer>
    {store => <RegisterContainer width={width} store={store} />}
  </StoreConsumer>;
}

import React from "react";
import useWindowDimensions from "../Context/useWindowDimensions";
import HomeContainer from '../containers/homeContainer';
import { StoreConsumer } from "../Context";

export default function HomeView(props: any) {
  const { width } = useWindowDimensions();
  return <StoreConsumer>
    {store => <HomeContainer width={width} store={store} />}
  </StoreConsumer>;
}

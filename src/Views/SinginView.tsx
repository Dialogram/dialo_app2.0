import React from "react";
import { StoreConsumer } from "../Context";
import SigninContainer from "../containers/signinContainer";
import useWindowDimensions from "../Context/useWindowDimensions";

export default function SigninView(props: any) {
  const { width } = useWindowDimensions();
  return <StoreConsumer>
    {store => <SigninContainer width={width} store={store} />}
  </StoreConsumer>;
}

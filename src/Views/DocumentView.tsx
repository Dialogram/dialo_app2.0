import React from "react";
import useWindowDimensions from "../Context/useWindowDimensions";
import DocumentContainer from '../containers/documentContainer';
import { StoreConsumer } from "../Context";

export default function DocumentView(props: any) {
  const { width } = useWindowDimensions();
  return <StoreConsumer>
    {store => <DocumentContainer width={width} store={store} />}
  </StoreConsumer>;
}

import React from "react";
import useWindowDimensions from "../Context/useWindowDimensions";

import { StoreConsumer } from "../Context";
import PasswordForgetSendContainer from '../containers/passwordForgetSendContainer';


export default function PasswordForgetView(props: any) {
    const { width } = useWindowDimensions();
    return <StoreConsumer>
        {store => <PasswordForgetSendContainer width={width} store={store} />}
    </StoreConsumer>;
}

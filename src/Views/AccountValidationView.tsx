import React from "react";
import useWindowDimensions from "../Context/useWindowDimensions";

import { StoreConsumer } from "../Context";
import AccountValidationContainer from "../containers/accountValidationContainer";

export default function AccountValidationView(props: any) {
    const { width } = useWindowDimensions();
    return <StoreConsumer>
        {store => <AccountValidationContainer width={width} store={store} />}
    </StoreConsumer>;
}

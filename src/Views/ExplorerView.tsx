import React from "react";
import useWindowDimensions from "../Context/useWindowDimensions";
import ExplorerContainer from '../containers/explorerContainer';
import { StoreConsumer } from "../Context";

export default function ExplorerView(props: any) {
  const { width } = useWindowDimensions();
  return <StoreConsumer>
    {store => <ExplorerContainer width={width} store={store} />}
  </StoreConsumer>;
}

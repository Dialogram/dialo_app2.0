import React, { Component, ReactNode } from "react";
import Store from "../Models/store";
import Session from '../Models/Session';
import User from "../Models/User";
import Video from '../Models/Video';

import Doc from '../Models/Document';
import Traduction from "../Models/Traduction";

import DialogramApi from '../Api/DialogramApi';
import Zone from "../Models/Zone";

import socketIOClient from "socket.io-client";
import { async } from "q";


const StoreContext = React.createContext({
  socket: undefined,
  user: new User({}),
  session: new Session({}),
  userDocument: new Array<Doc>(),
  userVideo: new Array<Video>(),
  isConnected: false,
  token: '',
  setSession: () => { },
  createSession: async () => { },
  getUser: async () => { },
  exploreDocument: () => { },
  getMyDocument: () => { },
  getSingleDocument: () => { },
  createUser: () => { },
  logout: () => { },
  getMasterTraduction: async () => { },
  updateDocument: async () => { },
  getUserVideo: async () => { },
  deleteVideo: async () => { },
  deleteDocument: async () => { },
  updateZone: async () => { },
  deleteZone: async () => { },
  createZone: async () => { },
  searchDocumentByName: async () => { },
  updatePublicInfo: async () => { },
  updateAccountInfo: async () => { },
  updatePassword: async () => { },
  likeDocument: async () => { },
  unLikeDocument: async () => { },
  confirmAccount: async () => { },
  sendConfirmAccount: async () => { },
  promoteUser: async () => { },
  searchUser: async () => { },
  getUserById: async () => { },
  demoteUser: async () => { },
  forgetPassword: async () => { },
  resetPasswordWithToken: async () => { }
});

type Props = {};

export const StoreConsumer = StoreContext.Consumer;

type PromiseResolve<T> = (value?: T | PromiseLike<T>) => void;
type PromiseReject = (error?: any) => void;

//socket.on("FromAPI", (data: any) => console.log(data));
//const BaseUrl = "http://localhost:8085/api/"; "https://api-dev.dialogram.fr/"
const BaseUrl = process.env.REACT_APP_BACKEND_HOST || "https://api-dev.dialogram.fr/";
const SocketUrl = process.env.REACT_APP_BACKEND_HOST || "https://api-dev.dialogram.fr/";
export { BaseUrl }
export default class StoreProvider extends Component<Props, Store> {
  constructor(props: Props) {
    super(props);
    this.state = {
      socket: socketIOClient(SocketUrl),
      user: new User({}),
      session: new Session({}),
      userDocument: new Array<Doc>(),
      userVideo: new Array<Video>(),
      isConnected: false,
      token: '',
      createSession: this.createSession,
      setSession: this.setSession,
      getUser: this.getUser,
      exploreDocument: this.exploreDocument,
      getMyDocument: this.getMyDocument,
      getSingleDocument: this.getSingleDocument,
      createUser: this.createUser,
      logout: this.logout,
      getMasterTraduction: this.getMasterTraduction,
      updateDocument: this.updateDocument,
      getUserVideo: this.getUserVideo,
      deleteVideo: this.deleteVideo,
      deleteDocument: this.deleteDocument,
      updateZone: this.updateZone,
      deleteZone: this.deleteZone,
      createZone: this.createZone,
      searchDocumentByName: this.searchDocumentByName,
      updatePublicInfo: this.updatePublicInfo,
      updateAccountInfo: this.updateAccountInfo,
      updatePassword: this.updatePassword,
      likeDocument: this.likeDocument,
      unLikeDocument: this.unLikeDocument,
      confirmAccount: this.confirmAccount,
      sendConfirmAccount: this.sendConfirmAccount,
      promoteUser: this.promoteUser,
      searchUser: this.searchUser,
      getUserById: this.getUserById,
      demoteUser: this.demoteUser,
      forgetPassword: this.forgetPassword,
      resetPasswordWithToken: this.resetPasswordWithToken,
    }
  }

  logout = () => {
    localStorage.clear();
    this.setState(
      {
        user: new User({}),
        session: new Session({}),
        userDocument: new Array<Doc>(),
        isConnected: false,
        token: '',
      }
    )
  }

  forgetPassword = (email: string) => {

    return DialogramApi.call<Session>({ method: 'POST', url: '/api/password/reset', body: { email: email } }).catch((err) => {
      return Promise.reject(err);
    }).then((s) => {

      return Promise.resolve(true);
    });
  }

  resetPasswordWithToken = (password: string, confirm: string, token: string) => {

    return DialogramApi.call<Session>({ method: 'POST', url: `/api/password/reset/${token}`, body: { password, confirm } }).catch((err) => {
      return Promise.reject(err);
    }).then((s) => {

      return Promise.resolve(true);
    });
  }

  setSession = (session: Session) => {
    localStorage.setItem('token', session.token as string);
    localStorage.setItem('userId', session.user as string);
    return this.setState({
      ...this.state,
      session,
    });
  }

  setUser = (user: User) => {
    return this.setState({
      ...this.state,
      isConnected: true,
      user: user
    });
  }

  confirmAccount = (token: string) => {
    return DialogramApi.call<User>({ method: 'GET', url: `/api/user/confirm/${token}` }, this.state.session.token).catch((err) => {
      console.log(err);
      return Promise.reject(err);
    }).then(async (z) => {
      return Promise.resolve(true);
    });
  }

  sendConfirmAccount = () => {
    return DialogramApi.call<User>({ method: 'PUT', url: `/api/user/send/verification` }, this.state.session.token).catch((err) => {
      console.log(err);
      return Promise.reject(err);
    }).then(async (z) => {
      return Promise.resolve(true);
    });
  }

  updatePublicInfo = async (body: any) => {
    return DialogramApi.call<User>({ method: 'PUT', url: `/api/user/settings/public`, body }, this.state.session.token).catch((err) => {
      console.log(err);
      return Promise.reject(err);
    }).then(async (z) => {
      console.log("z", z)
      const user = new User(z.data);
      this.setUser(user);
      return Promise.resolve(user);
    });
  }

  updateAccountInfo = async (body: any) => {
    return DialogramApi.call<User>({ method: 'PUT', url: `/api/user/settings/account`, body }, this.state.session.token).catch((err) => {
      console.log(err);
      return Promise.reject(err);
    }).then(async (z) => {
      const user = new User(z.data);
      this.setUser(user);
      return Promise.resolve(user);
    });
  }

  updatePassword = async (body: any) => {
    return DialogramApi.call<User>({ method: 'PUT', url: `/api/user/settings/password`, body }, this.state.session.token).catch((err) => {
      console.log(err);
      return Promise.reject(err);
    }).then(async (z) => {
      const user = new User(z.data);
      this.setUser(user);
      return Promise.resolve(user);
    });
  }

  getMasterTraduction = async (idDocument: string) => {
    return DialogramApi.call<Traduction>({ method: 'GET', url: `/api/document/${idDocument}/translation` }, this.state.session.token).catch((err) => {
      console.log("rejected traduction");
      return Promise.reject(err);
    }).then((t) => {
      const traduction = new Traduction(t.data);
      return traduction;
    });
  }

  updateZone = async (idDocument: string, idTraduction: string, idZone: string, body: { width: any, height: any }) => {
    body = {
      ...body,
      width: parseInt(body.width, 10),
      height: parseInt(body.height, 10)
    }
    return DialogramApi.call<Zone>({ method: 'PUT', url: `/api/translation/${idTraduction}/zone/${idZone}`, body }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then(async (z) => {
      const zone = new Zone(z.data);
      return Promise.resolve(zone);
    });
  }

  deleteZone = async (idDocument: string, idTraduction: string, idZone: string) => {
    return DialogramApi.call<Zone>({ method: 'DELETE', url: `/api/translation/${idTraduction}/zone/${idZone}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then(async () => {
      //TODO Get Selected traduction not master
      return Promise.resolve(true);
    });
  }

  createZone = async (idDocument: string, idTraduction: string, zone: Zone) => {
    zone._id = undefined;
    return DialogramApi.call<Zone>({ method: 'POST', url: `/api/translation/${idTraduction}/zone`, body: zone }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then(async (z) => {
      const zone = new Zone(z.data);
      return Promise.resolve(zone);
    });
  }

  createSession = async ({ email, password }: { email: string, password: string }) => {
    return DialogramApi.call<Session>({ method: 'POST', url: '/api/session', body: { email, password } }).catch((err) => {
      return Promise.reject(err);
    }).then(async (s) => {
      const session = new Session(s.data);
      this.setSession(session);
      await this.getUser();
      return Promise.resolve(session);
    });
  }

  createUser = ({ email, password, name, firstName, nickName }: { email: string, password: string, name: string, firstName: string, nickName: string }, success: any, error: any) => {
    let body = {
      "nickName": nickName,
      "profile": {
        "firstName": firstName,
        "lastName": name
      },
      "email": email,
      "password": password
    };
    return DialogramApi.call<User>({ method: 'POST', url: '/api/user', body }).catch((err) => {
      return Promise.reject(err);
    }).then(async (s: { data: User, includes: Array<Session> }) => {
      let includes = s.includes as Array<{}>;
      let session = new Session(includes[0]);
      let user = new User(s.data);
      this.setSession(session);
      this.setUser(user);
      return Promise.resolve(session);
    });
  }

  getUser = async () => {
    return DialogramApi.call<User>({ method: 'GET', url: '/api/user' }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((u) => {
      const user = new User(u.data);
      return this.setUser(user);
    });
  }

  getUserById = async (idUser: string) => {
    return DialogramApi.call<User>({ method: 'GET', url: `/api/user/profile/${idUser}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((u) => {
      const user = new User(u.data);
      return Promise.resolve(user);
    });
  }

  searchUser = async (toFind: string) => {
    return DialogramApi.call<User[]>({ method: 'GET', url: `/api/search/user/${toFind}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((u: { data: User[], includes: any }) => {
      const users = new Array<User>();
      for (let item of u.data as Array<Object>) {
        let usr = new User(item);
        users.push(usr);
      }
      return Promise.resolve(users);
    });
  }

  promoteUser = async (idDocument: string, idUser: string, role: string) => {
    return DialogramApi.call<Doc>({ method: 'PUT', url: `/api/document/${idDocument}/editAccess/${idUser}/${role}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d) => {
      const doc = new Doc(d.data);
      return Promise.resolve(doc);
    });
  }

  demoteUser = async (idDocument: string, idUser: string, role: string) => {
    return DialogramApi.call<Doc>({ method: 'DELETE', url: `/api/document/${idDocument}/editAccess/${idUser}/${role}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d) => {
      const doc = new Doc(d.data);
      return Promise.resolve(doc);
    });
  }

  likeDocument = async (document: Doc) => {
    return DialogramApi.call<Doc>({ method: 'PUT', url: `/api/document/${document.id}/like` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d) => {
      const doc = new Doc(d.data);
      return Promise.resolve(doc);
    });
  }

  unLikeDocument = async (document: Doc) => {
    return DialogramApi.call<Doc>({ method: 'PUT', url: `/api/document/${document.id}/unlike` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d) => {
      const doc = new Doc(d.data);
      return Promise.resolve(doc);
    });
  }

  exploreDocument = async ({ limit, page, category }: { limit: number, page: number, category: string }) => {
    return DialogramApi.call<Doc[]>({ method: 'GET', url: `/api/search/?limit=${limit}&page=${page}&category=${category}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d: { data: Array<any>, includes: Array<any> }) => {
      const documents = new Array<Doc>();
      if (Array.isArray(d.data) === true) {
        for (let item of d.data as Array<Object>) {
          let doc = new Doc(item);
          documents.push(doc);
        }
        return Promise.resolve(documents);
      }
    });
  }

  searchDocumentByName = async (name: string) => {
    console.log("search")
    return DialogramApi.call<Doc[]>({ method: 'GET', url: `/api/search/?limit=5&page=1&name=${name}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d: { data: Array<any>, includes: Array<any> }) => {
      console.log("search", d)
      const documents = new Array<Doc>();
      if (Array.isArray(d.data) === true) {
        for (let item of d.data as Array<Object>) {
          let doc = new Doc(item);
          documents.push(doc);
        }
        return Promise.resolve(documents);
      }
    });
  }

  getSingleDocument = (id: string) => {
    return DialogramApi.call<Doc>({ method: 'GET', url: `/api/document/${id}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d) => {
      const doc = new Doc(d.data);
      return Promise.resolve(doc);
    });
  }

  getMyDocument = async () => {
    return DialogramApi.call<Doc[]>({ method: 'GET', url: '/api/user/documents' }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d: { data: Array<any>, includes: Array<any> }) => {
      const documents = new Array<Doc>();
      if (Array.isArray(d.data) === true) {
        for (let item of d.data as Array<Object>) {
          let doc = new Doc(item);
          documents.push(doc);
        }
       this.setState({
          ...this.state,
          userDocument: documents
        })
        return Promise.resolve(documents);
      }
    });
  }

  updateDocument = async (id: string, body: any) => {
    return DialogramApi.call<Traduction>({ method: 'PUT', url: `/api/document/update/${id}`, body }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((d) => {
      const doc = new Doc(d.data);
      return Promise.resolve(doc);
    });
  }

  deleteDocument = async (id: string) => {
    return DialogramApi.call<Traduction>({ method: 'DELETE', url: `/api/document/${id}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then(() => {
      this.getMyDocument();
      return Promise.resolve(true);
    });
  }

  getUserVideo = async () => {
    return DialogramApi.call<Video[]>({ method: 'GET', url: '/api/user/translationVideos/' }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then((v: { data: Array<any>, includes: Array<any> }) => {
      const videos = new Array<Video>();
      if (Array.isArray(v.data) === true) {
        for (let item of v.data as Array<Object>) {
          let doc = new Video(item);
          videos.push(doc);
        }
        return this.setState({
          ...this.state,
          userVideo: videos
        })
      }
    });
  };

  deleteVideo = async (idApiVideo: string) => {
    return DialogramApi.call<any>({ method: 'DELETE', url: `/api/translationVideo/${idApiVideo}` }, this.state.session.token).catch((err) => {
      return Promise.reject(err);
    }).then(async (d) => {
      await this.getUserVideo();
      return Promise.resolve(true);
    });
  }

  render(): ReactNode {
    return (
      <StoreContext.Provider value={this.state} >
        {this.props.children}
      </StoreContext.Provider >
    );
  }
}

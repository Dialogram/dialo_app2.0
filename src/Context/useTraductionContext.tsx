import { useState } from 'react';
import Zone from '../Models/Zone';

interface State {
    zones: Array<Zone>,
    actualPage: number;
    selectedZone: string;
}

export default function useTraductionContext() {
    const [traductions, setTraduction] = useState<State>({
        zones: [],
        actualPage: 1,
        selectedZone: "0"
    });

    const pushZone = (zone: Zone): void => {
        const zones = traductions.zones;
        zones.push(zone);
        setTraduction({
            ...traductions,
            zones: zones
        });
    }

    const pushArrayZone = (zoneArray: Array<Zone>): Array<Zone> => {
        const zones = []
        for (const z of zoneArray) {
            zones.push(new Zone(z));
        }
        return zones;
    }

    const deleteZone = (zoneId: string) => {
        const zones = traductions.zones;
        const index = zones.findIndex((element: Zone) => {
            return element._id === zoneId;
        })
        zones.splice(index, 1);
        setTraduction({
            ...traductions,
            zones: zones
        })
    }

    const getZone = (zoneId: string): Zone | undefined => {
        const zone = traductions.zones.find((element: Zone) => {
            return element._id === zoneId;
        })
        return zone;
    }

    const selectZone = (zoneId: string) => {
        setTraduction({
            ...traductions,
            selectedZone: zoneId
        })
    }

    const clearZones = () => {
        setTraduction({
            ...traductions,
            zones: []
        })
    }

    const refreshZone = (zoneIdBefore: string, nextZone: Zone): void => {
        const index = traductions.zones.findIndex((element: Zone) => {
            return element._id === zoneIdBefore;
        })
        const zones = traductions.zones;
        if (index !== -1) {
            zones[index] = nextZone;
            setTraduction({
                ...traductions,
                zones: zones
            });
        } else {
            pushZone(nextZone);
        }

    }

    const refreshZoneArray = (zoneToAdd: Array<Zone>) => {
        const zones = traductions.zones;
        const temp = zones.filter((element) => {
            return element.state === "TEMP";
        })
        const array = pushArrayZone(zoneToAdd)
        for (let z of temp) {
            array.push(z);
        }
        setTraduction({
            ...traductions,
            zones: array
        })
    }

    const dragStop = (id: string, newX: number, newY: number) => {
        console.log("dragstop", newX, newY)
        const trad = traductions.zones
        const zone = getZone(id);
        if (zone !== undefined) {
            zone.x = toPercent(1280, newX);
            zone.y = toPercent(1810, newY);
            setTraduction({
                ...traductions,
                zones: trad
            })
        }
    }

    const resize = (ref: any, position: { x: number, y: number }, id: string) => {
        const trad = traductions.zones
        console.log(position.x, position.y, ref.style.width, ref.style.height);
        console.log("trad", trad)
        const zone = getZone(id);
        if (zone !== undefined) {
            zone.x = toPercent(1280, position.x);
            zone.y = toPercent(1810, position.y);
            zone.width = toPercent(1280, parseInt(ref.style.width));
            zone.height = toPercent(1810, parseInt(ref.style.height));
            setTraduction({
                ...traductions,
                zones: trad
            })
        }
    }

    const toPercent = (len: number, value: number) => {
        return value * 100 / len;
    }

    const toPixel = (len: number, value: number) => {
        return value * len / 100;
    }

    return {
        toPercent,
        toPixel,
        traductions,
        refreshZoneArray,
        refreshZone,
        clearZones,
        getZone,
        deleteZone,
        pushArrayZone,
        pushZone,
        selectZone,
        dragStop,
        resize
    }
}
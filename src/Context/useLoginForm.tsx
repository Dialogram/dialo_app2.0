import { useState } from 'react';


export default function useLoginForm() {
    const [form, setForm] = useState({ email: "", password: "", emailValid: false, passwordValid: false, emailError: "", passwordError: "", formError: "" });

    const handleChangeEmail = (e: any) => {
        const { value } = e.target;
        setForm({
            ...form,
            email: value,
            formError: ""
        })
    }
    const handleChangePassword = (e: any) => {
        const { value } = e.target;
        setForm({
            ...form,
            password: value,
            formError: ""
        })
    }
    const onBlurEmail = (e: any) => {
        const { value } = e.target;
        validField('email', value)

    }
    const onBlurPassword = (e: any) => {
        const { value } = e.target;
        validField('password', value)
    }


    const validField = (name: string, value: any) => {

        switch (name) {
            case 'email':
                if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
                    setForm({
                        ...form,
                        emailValid: false,
                        email: value,
                        emailError: "L'adresse e-mail est mal formée."
                    })
                } else {
                    setForm({
                        ...form,
                        emailValid: true,
                        email: value,
                        emailError: ""
                    })
                }
                break;
            case 'password':
                if (value.length <= 8) {
                    setForm({
                        ...form,
                        passwordValid: false,
                        password: value,
                        passwordError: "Le mot de passe doit faire 8 caracètres minimum."
                    })
                } else {
                    setForm({
                        ...form,
                        passwordValid: true,
                        password: value,
                        passwordError: ""
                    })
                }
                break;
        }
    }
    return {
        form,
        handleChangeEmail,
        handleChangePassword,
        onBlurPassword,
        onBlurEmail,
        setForm,
        validField
    }
}
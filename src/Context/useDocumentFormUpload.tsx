import { useState } from 'react';


export default function useDocumentFormUpload() {

    const [form, setForm] = useState({
        name: "",
        description: "",
        public: true,
        category: "administrative",
        file: "",
        nameValid: false,
        descriptionValid: false,
        categoryValid: false,
        fileValid: false,
        fileError: "",
        nameError: "",
        descriptionError: "",
        categoryError: false,
        formError: ""
    });

    const handleChange = (e: any) => {
        const { value, name } = e.target;

        setForm({
            ...form,
            [name]: value,
            formError: ""
        })
    }

    const handleTargetChecked = (e: any) => {
        const { name, checked } = e.target;
        setForm({
            ...form,
            [name]: checked
        })
    }

    const handleChangeSelected = (e: string) => {

        setForm({
            ...form,
            category: e
        })
    }

    const onBlur = (e: any) => {
        const { value, name } = e.target;
        validField(name, value)

    }
    
    const validField = (name: string, value: any) => {

        switch (name) {
            case 'name':
                if (value.length <= 0) {
                    setForm({
                        ...form,
                        nameValid: false,
                        name: value,
                        nameError: "Le nom ne peux pas être vide"
                    })
                } else {
                    setForm({
                        ...form,
                        nameValid: true,
                        name: value,
                        nameError: ""
                    })
                }
                break;
            case 'description':
                if (value.length <= 0) {
                    setForm({
                        ...form,
                        descriptionValid: false,
                        description: value,
                        descriptionError: "La description ne peux pas etre vide"
                    })
                } else {
                    setForm({
                        ...form,
                        descriptionValid: true,
                        description: value,
                        descriptionError: ""
                    })
                }
                break;
            case 'file':
                if (value.length <= 0) {
                    setForm({
                        ...form,
                        fileValid: false,
                        file: value,
                        fileError: "Le fichier ne peut être vide"
                    })
                } else {
                    setForm({
                        ...form,
                        fileValid: true,
                        file: value,
                        fileError: ""
                    })
                }
                break;
        }
    }
    return {
        form,
        handleChange,
        handleTargetChecked,
        onBlur,
        setForm,
        handleChangeSelected
    }
}
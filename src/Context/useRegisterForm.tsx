import { useState } from 'react';


export default function useRegisterForm() {
    const [form, setForm] = useState({
        email: "",
        password: "",
        name: "",
        firstName: "",
        nickName:"",
        nickNameValid:"",
        nickNameError:"",
        emailValid: false,
        passwordValid: false,
        nameVaid: false,
        firstNameValid: false,
        emailError: "",
        passwordError: "",
        nameError: "",
        firstNameError: "",
        formError: ""
    });

    const handleChange = (e: any) => {
        const { value, name } = e.target;
        setForm({
            ...form,
            [name]: value,
            formError: ""
        })
    }

    const onBlur = (e: any) => {
        const { value, name } = e.target;
        validField(name, value)

    }


    const validField = (name: string, value: any) => {

        switch (name) {
            case 'email':
                if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
                    setForm({
                        ...form,
                        emailValid: false,
                        email: value,
                        emailError: "L'adresse e-mail est mal formée."
                    })
                } else {
                    setForm({
                        ...form,
                        emailValid: true,
                        email: value,
                        emailError: ""
                    })
                }
                break;
            case 'password':
                if (value.length <= 8) {
                    setForm({
                        ...form,
                        passwordValid: false,
                        password: value,
                        passwordError: "Le mot de passe doit faire 8 caracètres minimum."
                    })
                } else {
                    setForm({
                        ...form,
                        passwordValid: true,
                        password: value,
                        passwordError: ""
                    })
                }
                break;
        }
    }
    return {
        form,
        handleChange,
        onBlur,
        setForm
    }
}
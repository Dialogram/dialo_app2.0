import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

import { StoreProvider } from "./Context";
import { StoreConsumer } from "./Context";
ReactDOM.render(
  <StoreProvider>
    <StoreConsumer>
      {store => <App store={store} />}
    </StoreConsumer>

  </StoreProvider>,
  document.getElementById("root")
);

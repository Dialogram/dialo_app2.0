/// <reference types="Cypress" />

describe('Login', function () {
    it('should fail email', function () {
      cy.visit('http://localhost:3000/login');
  
      cy.get('#email').type("francois.beche@epitech.eu1");
      cy.get('#password').type('zouzou100');
      cy.get('#submit').click();
      cy.wait(1000)
      cy.get("#email").clear();
      cy.get('#password').clear();
  
      cy.get('#email').type("francois.beche@epitech.eu");
      cy.get('#password').type('zouzou1001');
      cy.get('#submit').click();
      cy.wait(1000)
      cy.get("#email").clear();
      cy.get('#password').clear();
  
      cy.get('#email').type("francois.beche@epitech.eu");
      cy.get('#password').type('zouzou100');
      cy.get('#submit').click();
      cy.wait(1000)
    });
  });
  